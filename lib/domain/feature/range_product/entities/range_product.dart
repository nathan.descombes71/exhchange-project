import 'package:lug_front/domain/feature/product/entities/product.dart';
import 'package:lug_front/domain/feature/type_product/entities/type_product.dart';
import 'package:lug_front/domain/feature/user_infos/controller/user_infos_controller.dart';

import '../../../../infrastructure/dtos/range_product_dto.dart';
import '../../../../infrastructure/dtos/type_product_dto.dart';
import '../../../../infrastructure/dtos/user_infos_dto.dart';

class RangeProduct {
  int? id;
  String? pathPhoto;
  int? userId;
  int? typeProductId;
  List<Product>? listProduct;
  TypeProduct? typeProduct;

  RangeProduct({
    this.id,
    this.pathPhoto,
    this.listProduct,
    this.typeProduct,
    this.typeProductId,
    this.userId,
  });

  RangeProductDto get toDto {
    return RangeProductDto(
      id: id ?? 0,
      pathPhoto: pathPhoto,
      listProduct: listProduct?.toDto,
      typeProduct: typeProduct?.toDto,
      typeProductId: typeProductId,
      userId: userId,
    );
  }
}

extension OnRangeProduct on RangeProduct {
  RangeProduct copyWith({int? id, String? label, String? pathPhoto}) {
    return RangeProduct(
      id: id ?? this.id,
      pathPhoto: pathPhoto ?? this.pathPhoto,
    );
  }
}

extension OnListRangeProduct on List<RangeProduct> {
  List<RangeProductDto> get toDto {
    List<RangeProductDto> userInfosDtoList = [];

    this.forEach((entity) => userInfosDtoList.add(entity.toDto));
    return userInfosDtoList;
  }
}

extension OnListRangeProductDto on List<RangeProductDto> {
  List<RangeProduct> get toEntity {
    List<RangeProduct> userAppInfosList = [];

    this.forEach((dto) => userAppInfosList.add(dto.toEntity));
    return userAppInfosList;
  }
}

extension OnUserAppInfosDto on RangeProductDto {
  RangeProduct get toEntity {
    return RangeProduct(
      id: id,
      pathPhoto: pathPhoto,
      listProduct: listProduct?.toEntity,
      typeProduct: typeProduct?.toEntity,
      typeProductId: typeProductId,
      userId: userId,
    );
  }
}
