import 'package:lug_front/domain/feature/user_infos/controller/user_infos_controller.dart';

import '../../../../infrastructure/dtos/type_product_dto.dart';
import '../../../../infrastructure/dtos/user_infos_dto.dart';

class TypeProduct {
  int? id;
  String? label;
  String? pathPhoto;

  TypeProduct({
    this.id,
    this.label,
    this.pathPhoto,
  });

  TypeProductDto get toDto {
    return TypeProductDto(
      id: id ?? 0,
      pathPhoto: pathPhoto,
      label: label,
    );
  }
}

extension OnTypeProduct on TypeProduct {
  TypeProduct copyWith({int? id, String? label, String? pathPhoto}) {
    return TypeProduct(
      id: id ?? this.id,
      pathPhoto: pathPhoto ?? this.pathPhoto,
      label: label ?? this.label,
    );
  }
}

extension OnListTypeProduct on List<TypeProduct> {
  List<TypeProductDto> get toDto {
    List<TypeProductDto> userInfosDtoList = [];

    this.forEach((entity) => userInfosDtoList.add(entity.toDto));
    return userInfosDtoList;
  }
}

extension OnListTypeProductDto on List<TypeProductDto> {
  List<TypeProduct> get toEntity {
    List<TypeProduct> userAppInfosList = [];

    this.forEach((dto) => userAppInfosList.add(dto.toEntity));
    return userAppInfosList;
  }
}

extension OnUserAppInfosDto on TypeProductDto {
  TypeProduct get toEntity {
    return TypeProduct(
      id: id,
      pathPhoto: pathPhoto,
      label: label,
    );
  }
}
