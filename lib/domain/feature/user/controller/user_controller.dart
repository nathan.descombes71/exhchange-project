import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:lug_front/domain/feature/user_infos/entities/user_infos.dart';

import '../../../../../infrastructure/repositories/user_repository_impl.dart';
import '../entities/user.dart';

class UserController {
  final UserRepositoryImpl userRepository;

  UserController({
    required this.userRepository,
  });

  final box = GetStorage();

  Future<Either<String, User>> register(UserInfos userInfos) {
    return userRepository.register(userInfos.toDto).then(
          (value) => value.fold(
            (l) {
              return left(l);
            },
            (r) {
              return right(r.toEntity);
            },
          ),
        );
  }

  Future<Either<String, User>> login(String password, String mail) {
    return userRepository.login(password, mail).then(
          (value) => value.fold(
            (l) {
              return left(l);
            },
            (r) {
              box.write("jwt", r.jwt);
              Get.put<User>(User(userInfos: r.userInfos?.toEntity));
              return right(r.toEntity);
            },
          ),
        );
  }

  Future<Either<String, UserInfos>> me() {
    return userRepository.me().then(
          (value) => value.fold(
            (l) {
              return left(l);
            },
            (r) {
              return right(r.toEntity);
            },
          ),
        );
  }

  Future<Either<String, String>> logOut() {
    return userRepository.logOut().then(
          (value) => value.fold(
            (l) {
              return left(l);
            },
            (r) {
              box.remove("jwt");
              Get.delete<User>();
              return right(r);
            },
          ),
        );
  }

  Future<List<UserInfos>> indexProducteur() {
    return userRepository.indexProducteur().then(
          (value) => value.fold(
            (l) {
              return [];
            },
            (r) {
              return r.toEntity;
            },
          ),
        );
  }

  Future<Either<UserInfos?, UserInfos>> showUser(int userId) {
    return userRepository.showUser(userId).then(
          (value) => value.fold(
            (l) {
              return left(UserInfos());
            },
            (r) {
              return right(r.toEntity);
            },
          ),
        );
  }
}
