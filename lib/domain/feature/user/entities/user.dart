import 'package:lug_front/domain/feature/user_infos/controller/user_infos_controller.dart';
import 'package:lug_front/domain/feature/user_infos/entities/user_infos.dart';

import '../../../../infrastructure/dtos/user_dto.dart';

class User {
  UserInfos? userInfos;
  String? jwt;

  User({
    this.userInfos,
    this.jwt,
  });
}

extension OnUserInfos on User {
  User copyWith({
    UserInfos? userInfos,
    String? jwt,
  }) {
    return User(
      userInfos: userInfos ?? this.userInfos,
      jwt: jwt ?? this.jwt,
    );
  }

  UserDto get toDto {
    return UserDto(
      jwt: jwt,
      userInfos: userInfos?.toDto,
    );
  }
}

extension OnListUserAppInfos on List<User> {
  List<UserDto> get toDto {
    List<UserDto> userDtoList = [];

    this.forEach((entity) => userDtoList.add(entity.toDto));
    return userDtoList;
  }
}

extension OnListUserAppInfosDto on List<UserDto> {
  List<User> get toEntity {
    List<User> userAppInfosList = [];

    this.forEach((dto) => userAppInfosList.add(dto.toEntity));
    return userAppInfosList;
  }
}

extension OnUserDto on UserDto {
  User get toEntity {
    return User(
      jwt: jwt,
      userInfos: userInfos?.toEntity,
    );
  }
}
