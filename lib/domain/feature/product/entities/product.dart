import 'package:lug_front/domain/feature/range_product/entities/range_product.dart';
import 'package:lug_front/domain/feature/user_infos/controller/user_infos_controller.dart';

import '../../../../infrastructure/dtos/product_dto.dart';
import '../../../../infrastructure/dtos/type_product_dto.dart';
import '../../../../infrastructure/dtos/user_infos_dto.dart';

class Product {
  int? id;
  String? label;
  String? pathPhoto;
  String? description;
  double? prix;
  int? rangeProductId;
  RangeProduct? rangeProduct;

  Product({
    this.id,
    this.label,
    this.pathPhoto,
    this.description,
    this.prix,
    this.rangeProductId,
    this.rangeProduct,
  });

  ProductDto get toDto {
    return ProductDto(
      id: id ?? 0,
      pathPhoto: pathPhoto,
      label: label,
      description: description,
      prix: prix,
      rangeProduct: rangeProduct?.toDto,
      rangeProductId: rangeProductId,
    );
  }
}

extension OnProduct on Product {
  Product copyWith({
    int? id,
    String? label,
    String? pathPhoto,
    String? description,
    double? prix,
    int? rangeProductId,
    RangeProduct? rangeProduct,
  }) {
    return Product(
      id: id ?? this.id,
      pathPhoto: pathPhoto ?? this.pathPhoto,
      label: label ?? this.label,
      description: description ?? this.description,
      prix: prix ?? this.prix,
      rangeProduct: rangeProduct ?? this.rangeProduct,
      rangeProductId: rangeProductId ?? this.rangeProductId,
    );
  }
}

extension OnListProduct on List<Product> {
  List<ProductDto> get toDto {
    List<ProductDto> userInfosDtoList = [];

    this.forEach((entity) => userInfosDtoList.add(entity.toDto));
    return userInfosDtoList;
  }
}

extension OnListProductDto on List<ProductDto> {
  List<Product> get toEntity {
    List<Product> userAppInfosList = [];

    this.forEach((dto) => userAppInfosList.add(dto.toEntity));
    return userAppInfosList;
  }
}

extension OnUserAppInfosDto on ProductDto {
  Product get toEntity {
    return Product(
      id: id,
      pathPhoto: pathPhoto,
      label: label,
      description: description,
      prix: prix,
      rangeProduct: rangeProduct?.toEntity,
      rangeProductId: rangeProductId,
    );
  }
}
