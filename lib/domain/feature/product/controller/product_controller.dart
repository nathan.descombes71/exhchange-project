import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:lug_front/domain/feature/user_infos/entities/user_infos.dart';

import '../../../../../infrastructure/repositories/user_repository_impl.dart';
import '../../../../infrastructure/repositories/type_product_repository_impl.dart';
import '../entities/product.dart';

class TypeProductController {
  final TypeProductRepositoryImpl typeProductRepository;

  TypeProductController({
    required this.typeProductRepository,
  });
}
