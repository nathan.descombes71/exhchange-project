import 'package:flutter/material.dart';

import '../../../../infrastructure/repositories/user_infos_repository_impl.dart';

class UserInfosController {
  final UserInfosRepositoryImpl? userRepository;

  UserInfosController({
    @required this.userRepository,
  });
}
