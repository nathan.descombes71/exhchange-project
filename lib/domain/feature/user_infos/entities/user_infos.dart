import 'package:lug_front/domain/feature/range_product/entities/range_product.dart';
import 'package:lug_front/domain/feature/user_infos/controller/user_infos_controller.dart';

import '../../../../infrastructure/dtos/user_infos_dto.dart';

class UserInfos {
  int? id;
  String? nom;
  String? prenom;
  String? email;
  String? pathPhoto;
  String? adresse;
  double? lat;
  double? lng;
  String? description;
  String? telephone;
  String? password;
  int? type;
  List<RangeProduct>? listRangeProduct;

  String get fullname => '${prenom ?? ''} ${nom?.toUpperCase() ?? ''}';

  String get typeName => type == 1
      ? "Commerçant"
      : type == 2
          ? "Producteur"
          : "Administrateur";

  UserInfosDto get toDto {
    return UserInfosDto(
      id: id ?? 0,
      adresse: adresse,
      description: description,
      lat: lat,
      lng: lng,
      nom: nom,
      prenom: prenom,
      password: password,
      pathPhoto: pathPhoto,
      telephone: telephone,
      type: type,
      email: email,
      listRangeProduct: listRangeProduct?.toDto,
    );
  }

  UserInfos({
    this.id,
    this.prenom,
    this.nom,
    this.email,
    this.pathPhoto,
    this.adresse,
    this.lat,
    this.lng,
    this.description,
    this.type,
    this.password,
    this.telephone,
    this.listRangeProduct,
  });
}

extension OnUserInfos on UserInfos {
  UserInfos copyWith({
    int? id,
    String? firstName,
    String? lastName,
    String? email,
    String? pathPhoto,
    String? adresse,
    String? description,
    int? type,
    double? lat,
    double? lng,
    String? jwt,
    String? password,
    String? telephone,
    List<RangeProduct>? listRangeProduct,
  }) {
    return UserInfos(
      id: id ?? this.id,
      prenom: firstName ?? this.prenom,
      nom: lastName ?? this.nom,
      email: email ?? this.email,
      pathPhoto: pathPhoto ?? this.pathPhoto,
      description: description ?? this.description,
      type: type ?? this.type,
      adresse: adresse ?? this.adresse,
      lat: lat ?? this.lat,
      lng: lng ?? this.lng,
      password: password ?? this.password,
      telephone: telephone ?? this.telephone,
      listRangeProduct: listRangeProduct ?? this.listRangeProduct,
    );
  }
}

extension OnListUserInfos on List<UserInfos> {
  List<UserInfosDto> get toDto {
    List<UserInfosDto> userInfosDtoList = [];

    this.forEach((entity) => userInfosDtoList.add(entity.toDto));
    return userInfosDtoList;
  }
}

extension OnListUserInfosDto on List<UserInfosDto> {
  List<UserInfos> get toEntity {
    List<UserInfos> userAppInfosList = [];

    this.forEach((dto) => userAppInfosList.add(dto.toEntity));
    return userAppInfosList;
  }
}

extension OnUserAppInfosDto on UserInfosDto {
  UserInfos get toEntity {
    return UserInfos(
      id: id,
      adresse: adresse,
      nom: nom,
      prenom: prenom,
      description: description,
      email: email,
      lat: lat,
      lng: lng,
      password: password,
      pathPhoto: pathPhoto,
      telephone: telephone,
      type: type,
      listRangeProduct: listRangeProduct?.toEntity,
    );
  }
}
