import 'package:lug_front/presentation/core/helpers/data_converter.dart';
import 'package:lug_front/presentation/core/scaffold/n_scaffold.dart';
import 'package:lug_front/presentation/core/styles/styles.dart';
import 'package:lug_front/presentation/core/widgets/button_style.dart';
import 'package:lug_front/presentation/navigation/routes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../domain/feature/user/entities/user.dart';
import '../../../core/widgets/n_button.dart';
import 'profile_producteur_view_controller.dart';

class ProfileProducteurView extends GetView<ProfileProducteurViewController> {
  ProfileProducteurView() : super();

  @override
  Widget build(BuildContext context) {
    return controller.obx((state) => _buildContent(context));
  }

  Widget _buildContent(BuildContext context) {
    return controller.obx((state) => NScaffold(
            body: Container(
          constraints: BoxConstraints(minHeight: Get.height),
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.bottomLeft,
              end: Alignment.topRight,
              colors: [
                Colors.white,
                Get.theme.colorScheme.secondary.withOpacity(0.1),
                Colors.white,
              ],
            ),
          ),
          child: LayoutBuilder(builder: (context, boxConstraint) {
            return Column(children: [
              cardAbout(),
            ]);
          }),
        )));
  }

  Widget cardAbout() {
    return Padding(
      padding: const EdgeInsets.all(50.0),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        color: Colors.white,
        child: Stack(
          children: [
            Padding(
              padding: const EdgeInsets.all(30.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    // crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width: 150,
                        height: 150,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              image: NetworkImage(controller
                                      .producteur?.pathPhoto ??
                                  "https://firebasestorage.googleapis.com/v0/b/lug-data-c44d5.appspot.com/o/pdpUser%2FPdpbasique.png?alt=media&token=31144882-a1c8-4e29-a45d-1a4201500ab2"),
                              fit: BoxFit.cover),
                        ),
                      ),
                      SizedBox(
                        width: 40,
                      ),
                      Center(
                        child: Text(
                          controller.producteur?.fullname ?? "",
                          style: kTitleStyle.copyWith(
                              fontWeight: FontWeight.w600,
                              color: Get.theme.colorScheme.secondary),
                        ),
                      ),
                      SizedBox(
                        width: 100,
                      ),
                      Expanded(
                          flex: 2,
                          child: Align(
                            alignment: Alignment.topRight,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: 50,
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            "Adresse :",
                                            style: kSubTitleStyle,
                                          ),
                                          SizedBox(
                                            height: 10,
                                          ),
                                          Text(
                                            controller.producteur?.adresse ??
                                                "",
                                            style: ksubText,
                                          ),
                                        ],
                                      ),
                                    ),
                                    SizedBox(
                                      width: 50,
                                    ),
                                    Get.width > 1600
                                        ? Expanded(
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  "Mail :",
                                                  style: kSubTitleStyle,
                                                ),
                                                SizedBox(
                                                  height: 10,
                                                ),
                                                Text(
                                                  controller
                                                          .producteur?.email ??
                                                      "",
                                                  style: ksubText,
                                                ),
                                              ],
                                            ),
                                          )
                                        : SizedBox(),
                                    Get.width > 1600
                                        ? Expanded(
                                            child: SizedBox(),
                                          )
                                        : SizedBox(),
                                  ],
                                ),
                                SizedBox(
                                  height: 50,
                                ),
                                Row(
                                  children: [
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            "Téléphone :",
                                            style: kSubTitleStyle,
                                          ),
                                          SizedBox(
                                            height: 10,
                                          ),
                                          Text(
                                            DataConverter.phoneSpace(
                                              controller
                                                      .producteur?.telephone ??
                                                  "",
                                            ),
                                            style: ksubText,
                                          ),
                                        ],
                                      ),
                                    ),
                                    SizedBox(
                                      width: 50,
                                    ),
                                    Get.width < 1600
                                        ? Expanded(
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  "Mail :",
                                                  style: kSubTitleStyle,
                                                ),
                                                SizedBox(
                                                  height: 10,
                                                ),
                                                Text(
                                                  controller
                                                          .producteur?.email ??
                                                      "",
                                                  style: ksubText,
                                                ),
                                              ],
                                            ),
                                          )
                                        : SizedBox(),
                                  ],
                                ),
                              ],
                            ),
                          )),
                    ],
                  ),
                  SizedBox(
                    height: 25,
                  ),
                  Text(
                    "Description :",
                    style: kSubTitleStyle,
                  ),
                  SizedBox(
                    height: 25,
                  ),
                  Row(
                    children: [
                      Expanded(
                          child: Text(
                        controller.producteur?.description ?? "",
                        style: ksubText,
                      )),
                      Expanded(child: SizedBox())
                    ],
                  )
                ],
              ),
            ),
            Positioned(
              top: 50,
              right: 50,
              child: NButton(
                onPressed: () {},
                titleText: "Contactez le",
                backgroundColor: Colors.transparent,
                secondary: true,
                borderColor: Get.theme.colorScheme.primary.withOpacity(0.7),
                textStyle: TextStyle(
                    color: Get.theme.colorScheme.primary.withOpacity(0.7),
                    fontWeight: FontWeight.w600),
                widthBorder: 2,
                iconButton: Icons.chat_outlined,
              ),
            )
          ],
        ),
      ),
    );
  }
}
