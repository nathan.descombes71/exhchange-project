import 'package:lug_front/presentation/core/scaffold/n_scaffold.dart';
import 'package:lug_front/presentation/core/widgets/button_style.dart';
import 'package:lug_front/presentation/navigation/routes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'login_view_controller.dart';

class LoginView extends GetView<LoginViewController> {
  LoginView() : super();

  @override
  Widget build(BuildContext context) {
    return controller.obx((state) => _buildContent(context));
  }

  Widget _buildContent(BuildContext context) {
    return controller.obx(
      (state) => Scaffold(
        backgroundColor: Colors.white,
        body: Column(
          children: [
            Expanded(
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      "assets/logo/templogo.png",
                      height: 30,
                    ),
                    SizedBox(
                      height: 50,
                    ),
                    Container(
                      height: 40,
                      width: 250,
                      margin: EdgeInsets.fromLTRB(25, 0, 25, 0),
                      child: TextField(
                        controller: controller.mailOrPhoneController,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.zero,
                          prefixIcon: Icon(
                            Icons.mail,
                            color: Get.theme.colorScheme.secondary,
                          ),
                          border: OutlineInputBorder(),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      height: 40,
                      width: 250,
                      margin: EdgeInsets.fromLTRB(25, 0, 25, 0),
                      child: TextField(
                        controller: controller.passwordController,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.zero,
                          prefixIcon: Icon(
                            Icons.lock,
                            color: Get.theme.colorScheme.secondary,
                          ),
                          border: OutlineInputBorder(),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    ElevatedButton(
                      onPressed: () {
                        // controller.connexion();
                        controller.login();
                      },
                      child: Text(
                        "SE CONNECTER",
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                      ),
                      style: NButtonStyle.square(),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text("Mot de passe oublié ?"),
                        SizedBox(
                          width: 8,
                        ),
                        Text(
                          "Cliquez ici",
                          style: TextStyle(fontWeight: FontWeight.w800),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
            Divider(
              thickness: 1,
            ),
            Container(
                margin: EdgeInsets.fromLTRB(0, 30, 0, 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Pas encore de compte ?"),
                    SizedBox(
                      width: 8,
                    ),
                    GestureDetector(
                      onTap: () {
                        Get.toNamed(Routes.REGISTER);
                      },
                      child: Text(
                        "Cliquez ici",
                        style: TextStyle(fontWeight: FontWeight.w800),
                      ),
                    ),
                  ],
                ))
          ],
        ),
      ),
    );
  }
}
