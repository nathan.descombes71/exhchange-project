import 'package:lug_front/domain/feature/user/controller/user_controller.dart';
import 'package:lug_front/infrastructure/api/rest_api_client.dart';
import 'package:lug_front/infrastructure/repositories/user_infos_repository_impl.dart';
import 'package:get/get.dart';
import 'package:lug_front/infrastructure/repositories/user_repository_impl.dart';
import 'login_view_controller.dart';

class LoginViewControllerBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(
      () => LoginViewController(
        userController: Get.put(
          UserController(
            userRepository:
                UserRepositoryImpl(client: Get.find<RestApiClient>().client),
          ),
        ),
      ),
    );
  }
}
