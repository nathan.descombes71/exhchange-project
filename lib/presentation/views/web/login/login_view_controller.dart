import 'package:lug_front/domain/feature/user/controller/user_controller.dart';
import 'package:lug_front/domain/feature/user_infos/entities/user_infos.dart';
import 'package:lug_front/presentation/navigation/routes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../../../../domain/feature/user/entities/user.dart';

class LoginViewController extends GetxController with StateMixin {
  final UserController userController;

  LoginViewController({
    required this.userController,
  });

  PageController pageController = PageController();

  TextEditingController mailOrPhoneController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  final RxInt currentIndex = 0.obs;

  final box = GetStorage();

  @override
  void onInit() async {
    change(null, status: RxStatus.loading());

    super.onInit();
  }

  @override
  void onReady() async {
    change(state, status: RxStatus.success());

    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void refresh() {
    super.refresh();
  }

  void login() {
    userController
        .login("password", "nathan.descombes@live.fr")
        .then((value) => value.fold((l) {}, (r) {
              Get.toNamed(Routes.HOME);
            }));

    // Get.toNamed(Routes.REGISTER);
  }
}
