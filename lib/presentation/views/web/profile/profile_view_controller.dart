import 'package:lug_front/domain/feature/user/controller/user_controller.dart';
import 'package:lug_front/domain/feature/user_infos/entities/user_infos.dart';
import 'package:lug_front/presentation/navigation/routes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class ProfileViewController extends GetxController with StateMixin {
  ProfileViewController();

  PageController pageController = PageController();

  TextEditingController mailOrPhoneController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  final RxInt currentIndex = 0.obs;

  final box = GetStorage();

  @override
  void onInit() async {
    change(null, status: RxStatus.loading());

    super.onInit();
  }

  @override
  void onReady() async {
    change(state, status: RxStatus.success());

    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void refresh() {
    super.refresh();
  }

  void login() {
    // userController
    //     .register(
    //       UserInfos(
    //         adresse: "44 Route de channy",
    //         nom: "Badet",
    //         prenom: "Melvin",
    //         password: "password",
    //         email: "melvin.badet@yahoo.fr",
    //         type: 2,
    //         description: "Je suis description",
    //         telephone: "0612548796",
    //         pathPhoto: "test/test",
    //         lat: 45.9856,
    //         lng: 5.2354,
    //       ),
    //     )
    //     .then((value) => value.fold((l) {
    //           print("cest la merde");
    //         }, (r) {
    //           box.write("jwt", r.jwt);
    //           Get.toNamed(Routes.REGISTER);
    //         }));

    Get.toNamed(Routes.REGISTER);
  }
}
