import 'package:lug_front/presentation/core/scaffold/n_scaffold.dart';
import 'package:lug_front/presentation/core/widgets/button_style.dart';
import 'package:lug_front/presentation/navigation/routes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../domain/feature/user/entities/user.dart';
import 'profile_view_controller.dart';

class ProfileView extends GetView<ProfileViewController> {
  ProfileView() : super();

  @override
  Widget build(BuildContext context) {
    return controller.obx((state) => _buildContent(context));
  }

  Widget _buildContent(BuildContext context) {
    return controller.obx(
      (state) => Scaffold(
        backgroundColor: Colors.blue,
        body: Column(
          children: [
            Text(Get.find<User>().userInfos?.fullname ?? ""),
            Expanded(
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ElevatedButton(
                      onPressed: () {
                        Get.toNamed(Routes.HOME);
                        // controller.connexion();
                        // controller.login();
                      },
                      child: Text(
                        "SE CONNECTER",
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                      ),
                      style: NButtonStyle.square(),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
