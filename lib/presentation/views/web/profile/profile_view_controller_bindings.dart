import 'package:lug_front/domain/feature/user/controller/user_controller.dart';
import 'package:lug_front/infrastructure/api/rest_api_client.dart';
import 'package:lug_front/infrastructure/repositories/user_infos_repository_impl.dart';
import 'package:get/get.dart';
import 'package:lug_front/infrastructure/repositories/user_repository_impl.dart';
import 'profile_view_controller.dart';

class ProfileViewControllerBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(
      () => ProfileViewController(),
    );
  }
}
