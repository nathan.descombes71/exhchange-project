import 'package:lug_front/domain/feature/user/controller/user_controller.dart';
import 'package:lug_front/domain/feature/user_infos/entities/user_infos.dart';
import 'package:lug_front/presentation/navigation/routes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class HomeViewController extends GetxController with StateMixin {
  final UserController userController;

  HomeViewController(this.userController);

  PageController pageController = PageController();

  TextEditingController mailOrPhoneController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  final RxInt currentIndex = 0.obs;

  final box = GetStorage();

  @override
  void onInit() async {
    change(null, status: RxStatus.loading());

    print(Get.currentRoute);

    super.onInit();
  }

  @override
  void onReady() async {
    change(state, status: RxStatus.success());

    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void refresh() {
    super.refresh();
  }

  void logout() {
    userController.logOut().then((value) => value.fold((l) {}, (r) {
          Get.offAllNamed(Routes.REGISTER);
        }));
  }
}
