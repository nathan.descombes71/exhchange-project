import 'package:lug_front/presentation/core/scaffold/n_scaffold.dart';
import 'package:lug_front/presentation/core/widgets/button_style.dart';
import 'package:lug_front/presentation/navigation/routes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../domain/feature/user/entities/user.dart';
import 'home_view_controller.dart';

class HomeView extends GetView<HomeViewController> {
  HomeView() : super();

  @override
  Widget build(BuildContext context) {
    return controller.obx((state) => _buildContent(context));
  }

  Widget _buildContent(BuildContext context) {
    return controller.obx((state) => NScaffold(
            body: Container(
          child: Text(Get.find<User>().userInfos?.fullname ?? ""),
        )));
  }
}
