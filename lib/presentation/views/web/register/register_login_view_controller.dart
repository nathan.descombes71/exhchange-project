import 'package:lug_front/domain/feature/user/controller/user_controller.dart';
import 'package:lug_front/domain/feature/user/entities/user.dart';
import 'package:lug_front/domain/feature/user_infos/entities/user_infos.dart';
import 'package:lug_front/presentation/navigation/routes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class RegisterLoginViewController extends GetxController with StateMixin {
  final UserController? userController;

  RegisterLoginViewController({this.userController});

  PageController pageController = PageController();

  // Formfield Login Controller
  TextEditingController mailLoginController = TextEditingController();
  TextEditingController passwordLoginController = TextEditingController();

  // Formfield Register Controller
  TextEditingController mailRegisterController = TextEditingController();
  TextEditingController passwordRegisterController = TextEditingController();
  TextEditingController prenomController = TextEditingController();
  TextEditingController nomController = TextEditingController();
  TextEditingController adresseController = TextEditingController();

  RxDouble marginBottomLoginForm = 0.0.obs;
  RxDouble marginTopRegisterForm = 0.0.obs;

  TypeDropdown valueType = TypeDropdown(1, "Je suis producteur");

  List<TypeDropdown> typeDropdownList = [];

  RxBool isLogin = false.obs;

  @override
  void onInit() async {
    change(null, status: RxStatus.loading());
    typeDropdownList = [
      valueType,
      TypeDropdown(2, "Je suis commerçant"),
    ];

    marginBottomLoginForm.value = Get.height;
    super.onInit();
  }

  @override
  void onReady() async {
    change(state, status: RxStatus.success());

    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void refresh() {
    super.refresh();
  }

  changeForm() {
    if (isLogin.value) {
      marginTopRegisterForm.value = 0;
      marginBottomLoginForm.value = Get.height;
    } else {
      marginTopRegisterForm.value = Get.height;
      marginBottomLoginForm.value = 0;
    }

    isLogin.value = !isLogin.value;
  }

  void login() {
    userController
        ?.login(passwordLoginController.text, mailLoginController.text)
        .then((value) => value.fold((l) {}, (r) {
              Get.offAndToNamed(Routes.HOME);
            }));
  }

  void register() {
    // userController
    //     ?.login("password", "nathan.descombes@live.fr")
    //     .then((value) => value.fold((l) {}, (r) {
    //           Get.toNamed(Routes.HOME);
    //         }));
    print(passwordRegisterController.text);
    print(mailRegisterController.text);
    print(nomController.text);
    print(prenomController.text);
    print(adresseController.text);
    // Get.toNamed(Routes.REGISTER);
  }
}

class TypeDropdown {
  int id;
  String value;

  TypeDropdown(this.id, this.value);
}
