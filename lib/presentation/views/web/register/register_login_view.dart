import 'package:lug_front/presentation/core/scaffold/n_scaffold.dart';
import 'package:lug_front/presentation/core/styles/styles.dart';
import 'package:lug_front/presentation/core/widgets/button_style.dart';
import 'package:lug_front/presentation/core/widgets/n_button.dart';
import 'package:lug_front/presentation/navigation/routes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../core/widgets/n_textformfield.dart';
import 'register_login_view_controller.dart';

class RegisterLoginView extends GetView<RegisterLoginViewController> {
  RegisterLoginView() : super();

  @override
  Widget build(BuildContext context) {
    return controller.obx((state) => _buildContent(context));
  }

  Widget _buildContent(BuildContext context) {
    return controller.obx(
      (state) => Scaffold(
        backgroundColor: Colors.transparent,
        body: Row(
          children: [
            Container(
              height: Get.height,
              width: Get.width * 0.3,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.bottomLeft,
                  end: Alignment.topRight,
                  colors: [
                    Get.theme.colorScheme.primary,
                    Get.theme.colorScheme.secondary
                  ],
                ),
              ),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "LUG",
                      style: kLogoText.copyWith(color: Colors.white),
                    ),
                    SizedBox(
                      height: 100,
                    ),
                    Obx(() => Text(
                          controller.isLogin.value
                              ? "Pas encore de compte ?"
                              : "Déjà un compte ?",
                          style: kTextStyle.copyWith(
                              color: Colors.white,
                              fontSize: 25,
                              fontWeight: FontWeight.w300),
                        )),
                    SizedBox(
                      height: 25,
                    ),
                    Obx(() => NButton(
                          onPressed: () {
                            controller.changeForm();
                          },
                          titleText: controller.isLogin.value
                              ? "Page d'inscription"
                              : "Page de connexion",
                          backgroundColor: Colors.transparent,
                          secondary: true,
                        )),
                  ]),
            ),
            SizedBox(
              width: 150,
            ),
            Expanded(
              child: Container(
                child: Stack(
                  children: [
                    Obx(() => AnimatedPositioned(
                          curve: Curves.easeInToLinear,
                          top: controller.marginTopRegisterForm.value,
                          child: Container(
                              height: Get.height, child: registerForm()),
                          duration: Duration(milliseconds: 500),
                        )),
                    Obx(() => AnimatedPositioned(
                          curve: Curves.easeInToLinear,
                          bottom: controller.marginBottomLoginForm.value,
                          child:
                              Container(height: Get.height, child: loginForm()),
                          duration: Duration(milliseconds: 500),
                        )),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget loginForm() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        RichText(
            text: TextSpan(children: [
          TextSpan(
            text: "Se connecter à ",
            style:
                kTitleStyle.copyWith(fontSize: 60, fontWeight: FontWeight.w400),
          ),
          TextSpan(
            text: "LUG",
            style:
                kTitleStyle.copyWith(fontSize: 60, fontWeight: FontWeight.w700),
          )
        ])),
        SizedBox(
          height: Get.height * 0.1,
        ),
        Form(
          child: Container(
              width: Get.width * 0.25,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  NTextFormField(
                    placeholder: "Email",
                    textEditingController: controller.mailLoginController,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  NTextFormField(
                    placeholder: "Mot de passe",
                    textEditingController: controller.passwordLoginController,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                ],
              )),
        ),
        SizedBox(
          height: Get.height * 0.1,
        ),
        NButton(
          titleText: "Se connecter",
          onPressed: (() {
            controller.login();
          }),
        ),
      ],
    );
  }

  Widget registerForm() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        // SizedBox(
        //   height: 200,
        // ),
        RichText(
            text: TextSpan(children: [
          TextSpan(
            text: "Inscrivez-vous à ",
            style:
                kTitleStyle.copyWith(fontSize: 60, fontWeight: FontWeight.w400),
          ),
          TextSpan(
            text: "LUG",
            style:
                kTitleStyle.copyWith(fontSize: 60, fontWeight: FontWeight.w700),
          )
        ])),
        SizedBox(
          height: Get.height * 0.1,
        ),
        Form(
          child: Container(
              width: Get.width * 0.25,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Expanded(
                          child: NTextFormField(
                        placeholder: "Prénom",
                        textEditingController: controller.prenomController,
                      )),
                      SizedBox(
                        width: 20,
                      ),
                      Expanded(
                          child: NTextFormField(
                        placeholder: "Nom",
                        textEditingController: controller.nomController,
                      )),
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  NTextFormField(
                    placeholder: "Email",
                    textEditingController: controller.mailRegisterController,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  NTextFormField(
                    placeholder: "Mot de passe",
                    textEditingController:
                        controller.passwordRegisterController,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  NTextFormField(
                    placeholder: "Adresse",
                    textEditingController: controller.adresseController,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(18),
                        border: Border.all(
                          color: Colors.grey,
                        )),
                    width: Get.width * 0.25,
                    padding: EdgeInsets.only(left: 10, right: 20),
                    child: DropdownButton<TypeDropdown>(
                      dropdownColor: Colors.white,
                      isExpanded: true,
                      underline: Container(),
                      value: controller.valueType,
                      items: controller.typeDropdownList
                          .map<DropdownMenuItem<TypeDropdown>>(
                              (TypeDropdown value) {
                        return DropdownMenuItem<TypeDropdown>(
                          value: value,
                          child: Text(value.value),
                        );
                      }).toList(),
                      onChanged: (value) {
                        controller.valueType = value ?? controller.valueType;
                        controller.refresh();
                      },
                    ),
                  ),
                ],
              )),
        ),
        SizedBox(
          height: Get.height * 0.1,
        ),
        NButton(
          titleText: "S'inscrire",
          onPressed: (() {
            controller.register();
          }),
        ),
      ],
    );
  }
}
