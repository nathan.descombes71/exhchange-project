import 'package:lug_front/domain/feature/type_product/entities/type_product.dart';
import 'package:lug_front/domain/feature/user/controller/user_controller.dart';
import 'package:lug_front/domain/feature/user_infos/entities/user_infos.dart';
import 'package:lug_front/presentation/navigation/routes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../../../../domain/feature/type_product/controller/type_product_controller.dart';

class ListProducteurViewController extends GetxController with StateMixin {
  final TypeProductController typeProductController;
  final UserController userController;

  ListProducteurViewController(this.typeProductController, this.userController);

  PageController pageController = PageController();

  int lengthGridRangeProduct = 0;

  RxDouble rightPanel = (-200.0).obs;

  List<TypeProduct> listTypeProduct = [
    TypeProduct(id: 1, label: "Fromages"),
    TypeProduct(id: 2, label: "Viandes"),
    TypeProduct(id: 3, label: "Fruits"),
    TypeProduct(id: 4, label: "Légumes"),
    TypeProduct(id: 5, label: "Fromage"),
  ];

  TextEditingController mailOrPhoneController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  final RxInt currentIndex = 0.obs;

  final box = GetStorage();

  List<UserInfos> allProducteurs = [];

  @override
  void onInit() async {
    change(null, status: RxStatus.loading());

    listTypeProduct = await typeProductController.index();

    allProducteurs = await userController.indexProducteur();

    initLengthGridRangeProduct();

    super.onInit();
  }

  @override
  void onReady() async {
    change(state, status: RxStatus.success());

    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void refresh() {
    super.refresh();
  }

  initLengthGridRangeProduct() {
    if (Get.width >= 1648) {
      lengthGridRangeProduct = 4;
    } else if (Get.width >= 1286) {
      lengthGridRangeProduct = 3;
    } else if (Get.width >= 1266) {
      lengthGridRangeProduct = 2;
    } else {
      lengthGridRangeProduct = 4;
    }
  }

  onChangeWidth() {
    if (Get.width >= 1648) {
      lengthGridRangeProduct = 4;
    } else if (Get.width >= 1286) {
      lengthGridRangeProduct = 3;
    } else if (Get.width >= 1266) {
      lengthGridRangeProduct = 2;
    } else {
      lengthGridRangeProduct = 4;
    }
  }

  int changeLenghtTypeProduct(UserInfos producteur) {
    int lengthToReturn = 0;
    if (Get.width >= 1648) {
      (producteur.listRangeProduct?.length ?? 0) >= 4
          ? lengthToReturn = 4
          : lengthToReturn = producteur.listRangeProduct?.length ?? 0;
    } else if (Get.width >= 1286) {
      (producteur.listRangeProduct?.length ?? 0) >= 3
          ? lengthToReturn = 3
          : lengthToReturn = producteur.listRangeProduct?.length ?? 0;
    } else if (Get.width >= 1266) {
      (producteur.listRangeProduct?.length ?? 0) >= 2
          ? lengthToReturn = 2
          : lengthToReturn = producteur.listRangeProduct?.length ?? 0;
    } else {
      (producteur.listRangeProduct?.length ?? 0) >= 4
          ? lengthToReturn = 4
          : lengthToReturn = producteur.listRangeProduct?.length ?? 0;
    }

    return lengthToReturn;
  }

  deployRightPanel() {
    if (rightPanel.value == -200) {
      rightPanel.value = 0;
    } else {
      rightPanel.value = -200;
    }
  }
}
