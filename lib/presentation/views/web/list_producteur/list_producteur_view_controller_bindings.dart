import 'package:lug_front/domain/feature/type_product/controller/type_product_controller.dart';
import 'package:lug_front/domain/feature/user/controller/user_controller.dart';
import 'package:lug_front/infrastructure/api/rest_api_client.dart';
import 'package:lug_front/infrastructure/repositories/type_product_repository_impl.dart';
import 'package:lug_front/infrastructure/repositories/user_infos_repository_impl.dart';
import 'package:get/get.dart';
import 'package:lug_front/infrastructure/repositories/user_repository_impl.dart';
import 'list_producteur_view_controller.dart';

class ListProducteurViewControllerBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(
      () => ListProducteurViewController(
        Get.put<TypeProductController>(
          TypeProductController(
            typeProductRepository: Get.put<TypeProductRepositoryImpl>(
              TypeProductRepositoryImpl(
                  client: Get.find<RestApiClient>().client),
            ),
          ),
        ),
        Get.put<UserController>(
          UserController(
            userRepository: Get.put<UserRepositoryImpl>(
              UserRepositoryImpl(client: Get.find<RestApiClient>().client),
            ),
          ),
        ),
      ),
    );
  }
}
