import 'package:lug_front/domain/feature/type_product/entities/type_product.dart';
import 'package:lug_front/domain/feature/user_infos/entities/user_infos.dart';
import 'package:lug_front/presentation/core/scaffold/n_scaffold.dart';
import 'package:lug_front/presentation/core/styles/styles.dart';
import 'package:lug_front/presentation/core/widgets/button_style.dart';
import 'package:lug_front/presentation/core/widgets/n_textformfield.dart';
import 'package:lug_front/presentation/navigation/routes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lug_front/presentation/views/web/profile_producteur/profile_producteur_view_controller.dart';
import '../../../../domain/feature/user/entities/user.dart';
import 'list_producteur_view_controller.dart';

class ListProducteurView extends GetView<ListProducteurViewController> {
  ListProducteurView() : super();

  @override
  Widget build(BuildContext context) {
    return controller.obx((state) => _buildContent(context));
  }

  Widget _buildContent(BuildContext context) {
    return controller.obx((state) => NScaffold(
          body: LayoutBuilder(builder: (context, boxConstraints) {
            controller.onChangeWidth();
            return Container(
              constraints: BoxConstraints(minHeight: Get.height),
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.bottomLeft,
                  end: Alignment.topRight,
                  colors: [
                    Colors.white,
                    // Color.fromARGB(144, 173, 173, 173),
                    Get.theme.colorScheme.secondary.withOpacity(0.1),
                    Colors.white,
                  ],
                ),
              ),
              child: Stack(
                children: [
                  SingleChildScrollView(
                    child: Container(
                        padding: EdgeInsets.all(20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: 50,
                            ),
                            Text(
                              "Liste des producteurs",
                              style: kTitleStyle.copyWith(
                                  fontWeight: FontWeight.bold),
                            ),
                            SizedBox(
                              height: Get.height * 0.13,
                            ),
                            Row(
                              children: [
                                Container(
                                  width: 250,
                                  child: NTextFormField(
                                    placeholder: "Rechercher par nom",
                                    icon: Icons.search,
                                  ),
                                ),
                                Expanded(flex: 4, child: SizedBox()),
                              ],
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                    flex: 4,
                                    child: Container(
                                      child: GridView.builder(
                                          shrinkWrap: true,
                                          gridDelegate:
                                              const SliverGridDelegateWithMaxCrossAxisExtent(
                                                  maxCrossAxisExtent: 700,
                                                  mainAxisExtent: 350,
                                                  childAspectRatio: 3 / 2,
                                                  crossAxisSpacing: 20,
                                                  mainAxisSpacing: 20),
                                          itemCount:
                                              controller.allProducteurs.length,
                                          itemBuilder:
                                              (BuildContext ctx, index) {
                                            return producteurCard(controller
                                                .allProducteurs[index]);
                                          }),
                                    )),
                                SizedBox(
                                  width: 25,
                                ),
                                Get.width >= 1400
                                    ? Expanded(
                                        child: Container(
                                        padding: EdgeInsets.only(left: 15),
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius:
                                                BorderRadius.circular(18)),
                                        child: Column(children: [
                                          SizedBox(
                                            height: 25,
                                          ),
                                          Text(
                                            "Filtrer par type de produit",
                                            style: ksubText.copyWith(
                                              fontWeight: FontWeight.w700,
                                            ),
                                          ),
                                          SizedBox(
                                            height: 25,
                                          ),
                                          ListView.builder(
                                              shrinkWrap: true,
                                              itemCount: controller
                                                  .listTypeProduct.length,
                                              itemBuilder: ((context, index) {
                                                return Row(
                                                  children: [
                                                    SizedBox(
                                                      width: 25,
                                                    ),
                                                    Checkbox(
                                                      value: false,
                                                      onChanged: (value) {},
                                                      fillColor:
                                                          MaterialStateProperty
                                                              .all<Color>(
                                                                  Colors.black),
                                                    ),
                                                    SizedBox(
                                                      width: 15,
                                                    ),
                                                    Text(
                                                      controller
                                                              .listTypeProduct[
                                                                  index]
                                                              .label ??
                                                          "",
                                                      style:
                                                          kTextStyle.copyWith(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w600),
                                                    ),
                                                  ],
                                                );
                                              })),
                                          SizedBox(
                                            height: 25,
                                          )
                                        ]),
                                      ))
                                    : SizedBox(),
                              ],
                            ),
                          ],
                        )),
                  ),
                  Get.width <= 1399
                      ? Obx(
                          () => AnimatedPositioned(
                            right: controller.rightPanel.value,
                            top: Get.height * 0.3,
                            child: Row(
                              children: [
                                GestureDetector(
                                  onTap: () {
                                    controller.deployRightPanel();
                                  },
                                  child: Container(
                                    height: 50,
                                    width: 30,
                                    child: Center(
                                        child: Icon(
                                      Icons.filter_list,
                                      color: Colors.white,
                                    )),
                                    decoration: BoxDecoration(
                                        boxShadow: [
                                          BoxShadow(
                                            offset: Offset(0, 0),
                                            spreadRadius: -2,
                                            blurRadius: 12,
                                            color: Color.fromRGBO(0, 0, 0, 1),
                                          )
                                        ],
                                        color: Get.theme.colorScheme.primary,
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(5),
                                            bottomLeft: Radius.circular(5))),
                                  ),
                                ),
                                Container(
                                  // height: Get.height * 0.4,
                                  constraints: BoxConstraints(
                                      maxHeight: Get.height * 0.4),
                                  width: 200,
                                  padding: EdgeInsets.only(left: 15),
                                  decoration: BoxDecoration(
                                      boxShadow: [
                                        BoxShadow(
                                          offset: Offset(0, 0),
                                          spreadRadius: -2,
                                          blurRadius: 12,
                                          color: Color.fromRGBO(0, 0, 0, 1),
                                        )
                                      ],
                                      color: Get.theme.colorScheme.primary,
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(18),
                                          bottomLeft: Radius.circular(18))),
                                  child: SingleChildScrollView(
                                    child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          SizedBox(
                                            height: 25,
                                          ),
                                          Text(
                                            "Filtrer par type de produit",
                                            style: ksubText.copyWith(
                                                fontWeight: FontWeight.w600,
                                                color: Colors.white),
                                          ),
                                          SizedBox(
                                            height: 25,
                                          ),
                                          ListView.builder(
                                              shrinkWrap: true,
                                              itemCount: controller
                                                  .listTypeProduct.length,
                                              itemBuilder: ((context, index) {
                                                return Row(
                                                  children: [
                                                    SizedBox(
                                                      width: 25,
                                                    ),
                                                    Checkbox(
                                                        fillColor:
                                                            MaterialStateProperty
                                                                .all<Color>(
                                                                    Colors
                                                                        .white),
                                                        value: false,
                                                        onChanged: (value) {}),
                                                    SizedBox(
                                                      width: 15,
                                                    ),
                                                    Text(
                                                      controller
                                                              .listTypeProduct[
                                                                  index]
                                                              .label ??
                                                          "",
                                                      style:
                                                          kTextStyle.copyWith(
                                                              color:
                                                                  Colors.white),
                                                    ),
                                                  ],
                                                );
                                              })),
                                          SizedBox(
                                            height: 25,
                                          )
                                        ]),
                                  ),
                                ),
                              ],
                            ),
                            duration: Duration(milliseconds: 500),
                            curve: Curves.decelerate,
                          ),
                        )
                      : SizedBox()
                ],
              ),
            );
          }),
        ));
  }

  Widget producteurCard(UserInfos producteur) {
    RxBool isImageError = false.obs;
    return InkWell(
      onTap: (() {
        Get.delete<ProfileProducteurViewController>();
        Get.toNamed(Routes.PROFILEPRODUCTEUR + "/${producteur.id}");
      }),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(18.0),
        ),
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(30.0, 30, 30, 0),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Obx(() => Row(
                  children: [
                    if (!isImageError.value)
                      Container(
                        width: 75,
                        height: 75,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              onError: ((exception, stackTrace) {
                                isImageError.value = true;
                              }),
                              image: NetworkImage(producteur.pathPhoto ??
                                  "https://firebasestorage.googleapis.com/v0/b/lug-data-c44d5.appspot.com/o/pdpUser%2FPdpbasique.png?alt=media&token=31144882-a1c8-4e29-a45d-1a4201500ab2"),
                              fit: BoxFit.cover),
                        ),
                      ),
                    if (isImageError.value)
                      Icon(
                        Icons.account_circle,
                        size: 90,
                      ),
                    SizedBox(
                      width: 25,
                    ),
                    Expanded(
                      flex: 4,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        // mainAxisSize: MainAxisSize.max,
                        children: [
                          Text(
                            producteur.fullname,
                            style: kTitleStyle.copyWith(
                                fontWeight: FontWeight.w600),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            producteur.adresse ?? "",
                            style:
                                ksubText.copyWith(fontWeight: FontWeight.w500),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                        child: Container(
                      height: 50,
                      width: 50,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(
                              color: Get.theme.colorScheme.secondary,
                              width: 2)),
                      child: Icon(
                        Icons.mail_outline,
                        color: Get.theme.colorScheme.secondary,
                        size: 28,
                      ),
                    ))
                  ],
                )),
            SizedBox(
              height: 30,
            ),
            Text(
              "Description:",
              style: ksubText.copyWith(fontWeight: FontWeight.w600),
            ),
            SizedBox(
              height: 15,
            ),
            Container(
              height: 75,
              child: Text(
                producteur.description ?? "",
                maxLines: 3,
                overflow: TextOverflow.ellipsis,
                style: kTextStyle,
              ),
            ),
            Expanded(
              child: GridView.builder(
                  shrinkWrap: true,
                  itemCount: controller.changeLenghtTypeProduct(producteur),
                  gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                      maxCrossAxisExtent: 125,
                      mainAxisExtent: 35,
                      crossAxisSpacing: 20,
                      mainAxisSpacing: 20),
                  itemBuilder: (ctx, index) {
                    return Container(
                      height: 25,
                      width: 50,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color:
                              Get.theme.colorScheme.secondary.withOpacity(0.2)),
                      child: Center(
                          child: Text(
                        (index + 1) != controller.lengthGridRangeProduct
                            ? (producteur.listRangeProduct?[index].typeProduct
                                    ?.label ??
                                "")
                            : ((producteur.listRangeProduct?.length ?? 0) -
                                        (controller.lengthGridRangeProduct -
                                            1)) !=
                                    1
                                ? "+ " +
                                    ((producteur.listRangeProduct?.length ??
                                                0) -
                                            (controller.lengthGridRangeProduct -
                                                1))
                                        .toString() +
                                    " autres"
                                : (producteur.listRangeProduct?[index]
                                        .typeProduct?.label ??
                                    ""),
                        style: kTextStyle.copyWith(
                            fontWeight: FontWeight.w600,
                            color:
                                Get.theme.colorScheme.primary.withOpacity(0.6)),
                      )),
                    );
                  }),
            )
          ]),
        ),
      ),
    );
  }
}
