import 'package:lug_front/presentation/views/web/home/home_view.dart';
import 'package:lug_front/presentation/views/web/home/home_view_controller_bindings.dart';
import 'package:lug_front/presentation/views/web/list_producteur/list_producteur_view.dart';
import 'package:lug_front/presentation/views/web/login/login_view.dart';
import 'package:lug_front/presentation/views/web/login/login_view_controller_bindings.dart';
import 'package:flutter/foundation.dart';
import 'package:get/get.dart';
import 'package:lug_front/presentation/views/web/profile/profile_view.dart';
import 'package:lug_front/presentation/views/web/profile_producteur/profile_producteur_view.dart';
import 'package:lug_front/presentation/views/web/register/register_login_view.dart';
import 'package:lug_front/presentation/views/web/register/register_login_view_controller_bindings.dart';
import '../views/web/list_producteur/list_producteur_view_controller_bindings.dart';
import '../views/web/profile/profile_view_controller_bindings.dart';
import '../views/web/profile_producteur/profile_producteur_view_controller_bindings.dart';
import 'routes.dart';

class Nav {
  static List<GetPage> routes = [
    /// REVIEW [Mobile & Web] routes
    GetPage(
      name: Routes.LOGIN,
      page: () => LoginView(),
      binding: LoginViewControllerBindings(),
    ),
    GetPage(
      name: Routes.REGISTER,
      page: () => RegisterLoginView(),
      binding: RegisterLoginViewControllerBindings(),
    ),
    GetPage(
      name: Routes.HOME,
      page: () => HomeView(),
      binding: HomeViewControllerBindings(),
    ),
    GetPage(
      name: Routes.LISTPRODUCTEUR,
      page: () => ListProducteurView(),
      binding: ListProducteurViewControllerBindings(),
    ),
    GetPage(
      name: Routes.PROFILE,
      page: () => ProfileView(),
      binding: ProfileViewControllerBindings(),
    ),
    GetPage(
      name: Routes.PROFILEPRODUCTEUR + "/:idUser",
      page: () => ProfileProducteurView(),
      binding: ProfileProducteurViewControllerBindings(),
      preventDuplicates: false,
      maintainState: false,
    ),
  ];
}
