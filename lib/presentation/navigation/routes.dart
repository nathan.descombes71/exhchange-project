import 'package:flutter/foundation.dart';

class Routes {
  static String get initialRoute {
    // TODO: implement method
    return HOME;
  }

  static const HOME = '/home';
  static const PORTAL = '/portal';
  static const LOGIN = '/login';
  static const REGISTER = '/register';
  static const MAP = '/map';
  static const LISTPRODUCTEUR = '/listProducteur';
  static const PROFILE = '/profile';
  static const CUSTOMERSDETAILS = '/customersDetails';
  static const MYSHOP = '/myShop';
  static const PROFILEPRODUCTEUR = '/profile-producteur';
}
