import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../styles/styles.dart';

class NTextFormField extends StatefulWidget {
  final String? titleText;
  final String? placeholder;
  final Function(String value)? onChanged;
  final TextEditingController? textEditingController;
  final IconData? icon;

  NTextFormField({
    this.textEditingController,
    this.titleText,
    this.placeholder,
    this.onChanged,
    this.icon,
  }) : super();

  @override
  State<NTextFormField> createState() => _NTextFormFieldState();
}

class _NTextFormFieldState extends State<NTextFormField> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: widget.textEditingController,
      onChanged: widget.textEditingController == null
          ? (value) {
              widget.onChanged!(value);
            }
          : null,
      cursorColor: Colors.black,
      decoration: InputDecoration(
          prefixIcon: widget.icon != null ? Icon(widget.icon) : null,
          hoverColor: Colors.white,
          focusColor: Colors.white,
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(18)),
              borderSide: BorderSide(color: Colors.grey, width: 1)),
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(18)),
              borderSide: BorderSide(color: Colors.grey, width: 1)),
          fillColor: Colors.white,
          border: const OutlineInputBorder(
              borderSide: BorderSide(color: Colors.black, width: 1)),
          hintText: widget.placeholder ?? "",
          hintStyle: ksubText.copyWith(color: Colors.grey)),
    );
  }
}
