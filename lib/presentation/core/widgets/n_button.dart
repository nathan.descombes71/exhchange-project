import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../styles/styles.dart';

class NButton extends StatefulWidget {
  final String? titleText;
  final IconData? iconButton;
  final Function() onPressed;
  final Widget? iconWidget;
  final Color? backgroundColor;
  final bool secondary;
  final double? height;
  final Color borderColor;
  final TextStyle? textStyle;
  final double? widthBorder;

  NButton({
    required this.titleText,
    this.iconButton,
    required this.onPressed,
    this.backgroundColor,
    this.iconWidget,
    this.secondary = false,
    this.height,
    this.borderColor = Colors.white,
    this.textStyle,
    this.widthBorder,
  }) : super();

  @override
  State<NButton> createState() => _NButtonState();
}

class _NButtonState extends State<NButton> {
  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ButtonStyle(
        shadowColor: MaterialStateProperty.all<Color>(
            widget.backgroundColor ?? Get.theme.colorScheme.secondary),
        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
        textStyle: MaterialStateProperty.all<TextStyle>(ksubText),
        backgroundColor: MaterialStateProperty.all<Color>(
            widget.backgroundColor ?? Get.theme.colorScheme.secondary),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(18.0),
            side: widget.secondary
                ? BorderSide(
                    color: widget.borderColor, width: widget.widthBorder ?? 1)
                : BorderSide.none,
          ),
        ),
      ),
      child: Container(
        padding: EdgeInsets.fromLTRB(7, 7, 7, 10),
        // height: widget.height ?? 50,
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            if (widget.iconButton != null) ...[
              Padding(
                padding: const EdgeInsets.only(top: 2),
                child: Icon(
                  widget.iconButton,
                  color: Colors.black,
                ),
              ),
              SizedBox(
                width: 10,
              ),
            ],
            Text(
              widget.titleText ?? "",
              style: widget.textStyle,
            ),
          ],
        ),
      ),
      onPressed: widget.onPressed,
    );
  }
}
