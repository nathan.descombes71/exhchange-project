import 'package:flutter/material.dart';

class NButtonStyle extends ButtonStyle {
  final MaterialStateProperty<Color>? backgroundColor;
  final MaterialStateProperty<OutlinedBorder>? shape;
  final MaterialStateProperty<double>? elevation;
  final MaterialStateProperty<EdgeInsetsGeometry>? padding;

  NButtonStyle._({
    this.backgroundColor,
    this.shape,
    this.elevation,
    this.padding,
  }) : super(backgroundColor: backgroundColor, shape: shape, padding: padding);

  factory NButtonStyle.primary() {
    return NButtonStyle._(
      elevation: MaterialStateProperty.all<double>(0),
      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
              side: BorderSide(color: Colors.black))),
      backgroundColor:
          MaterialStateProperty.all<Color>(Colors.black.withOpacity(0.8)),
    );
  }

  factory NButtonStyle.secondary() {
    return NButtonStyle._(
      elevation: MaterialStateProperty.all<double>(0),
      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
              side: BorderSide(color: Colors.black))),
      backgroundColor: MaterialStateProperty.all<Color>(Colors.white),
    );
  }

  factory NButtonStyle.square() {
    return NButtonStyle._(
        elevation: MaterialStateProperty.all<double>(0),
        backgroundColor: MaterialStateProperty.all<Color>(Colors.black),
        padding: MaterialStateProperty.all<EdgeInsetsGeometry>(
            EdgeInsets.fromLTRB(50, 15, 50, 15)));
  }
}
