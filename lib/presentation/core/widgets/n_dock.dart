import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lug_front/presentation/navigation/routes.dart';

import '../../../domain/feature/user/entities/user.dart';
import '../styles/styles.dart';

class NDock extends StatefulWidget {
  double widthDock;

  NDock({
    required this.widthDock,
  }) : super();

  @override
  State<NDock> createState() => _NDockState();
}

class _NDockState extends State<NDock> {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, boxConstraints) {
      return Container(
        color: Get.theme.colorScheme.primary,
        height: Get.height,
        width: widget.widthDock,
        child: Padding(
          padding: EdgeInsets.fromLTRB(16, 20, 16, 20),
          child: Column(children: [
            headerProfile(),
            Divider(
              endIndent: 0,
              indent: 0,
              thickness: 1,
            ),
            Expanded(child: middleDiv()),
          ]),
        ),
      );
    });
  }

  Widget headerProfile() {
    RxBool isImageError = false.obs;
    return Container(
      margin: EdgeInsets.only(bottom: 25),
      child: Obx(
        () => Row(children: [
          if (!isImageError.value)
            Container(
              width: 75,
              height: 75,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                    onError: ((exception, stackTrace) {
                      print("test");
                      isImageError.value = true;
                    }),
                    image: NetworkImage(
                        Get.find<User>().userInfos?.pathPhoto ?? ""),
                    fit: BoxFit.cover),
              ),
            ),
          if (isImageError.value)
            Icon(
              Icons.account_circle,
              size: 75,
              color: Colors.white,
            ),
          SizedBox(
            width: 15,
          ),
          Column(
            children: [
              RichText(
                  text: TextSpan(children: [
                TextSpan(
                    text: (Get.find<User>().userInfos?.prenom ?? "") + " ",
                    style: ksubText.copyWith(color: Colors.white)),
                TextSpan(
                    text: Get.find<User>().userInfos?.nom?.toUpperCase(),
                    style: ksubText.copyWith(
                        color: Colors.white, fontWeight: FontWeight.w600))
              ])),
              SizedBox(
                height: 10,
              ),
              Text(
                Get.find<User>().userInfos?.typeName ?? "",
                style: ksubText.copyWith(color: Colors.white),
              ),
              SizedBox(
                height: 10,
              ),
              InkWell(
                onTap: (() {}),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Icon(
                      Icons.logout,
                      color: Colors.white,
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      "Deconnexion",
                      style: ksubText.copyWith(
                          color: Colors.white, fontWeight: FontWeight.w600),
                    ),
                  ],
                ),
              ),
            ],
          )
        ]),
      ),
    );
  }

  Widget middleDiv() {
    return Container(
      // color: Colors.red,
      child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        rowIcon(Icons.home, "Accueil", Routes.HOME),
        SizedBox(
          height: 50,
        ),
        rowIcon(Icons.map, "Carte producteurs", Routes.MAP),
        SizedBox(
          height: 50,
        ),
        rowIcon(Icons.list, "Liste producteurs", Routes.LISTPRODUCTEUR),
      ]),
    );
  }

  Widget rowIcon(IconData icon, String title, String nameRoute) {
    RxBool isHover = false.obs;

    return InkWell(
      onTap: () {
        // print(title);
        Get.toNamed(nameRoute);
      },
      onHover: (hovered) {
        isHover.value = hovered;
      },
      child: Obx(
        () => Container(
          decoration: BoxDecoration(
              color: Get.currentRoute == nameRoute
                  ? Get.theme.colorScheme.secondary.withOpacity(0.2)
                  : isHover.value
                      ? Get.theme.colorScheme.secondary.withOpacity(0.4)
                      : Colors.transparent,
              borderRadius: BorderRadius.circular(18)),
          padding: isHover.value ? EdgeInsets.all(3) : EdgeInsets.all(3),
          child: Row(
            children: [
              Icon(
                icon,
                color: Colors.white,
                size: 50,
              ),
              SizedBox(
                width: 10,
              ),
              Expanded(
                flex: 5,
                child: Text(
                  title,
                  style: ksubText.copyWith(color: Colors.white),
                ),
              ),
              Expanded(
                child: Align(
                  alignment: Alignment.centerRight,
                  child: Icon(
                    Icons.chevron_right,
                    color: Colors.white,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
