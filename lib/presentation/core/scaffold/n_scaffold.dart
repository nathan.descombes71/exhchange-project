import 'package:lug_front/presentation/core/widgets/n_app_bar.dart';
import 'package:lug_front/presentation/core/widgets/n_dock.dart';
import 'package:lug_front/presentation/navigation/routes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class NScaffold extends StatefulWidget {
  final Widget body;

  const NScaffold({
    required this.body,
  }) : super();

  @override
  State<NScaffold> createState() => _NScaffoldState();
}

class _NScaffoldState extends State<NScaffold> {
  double widthDock = 300;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.withOpacity(0.1),
      body: Container(
        child: Stack(
          children: [
            Positioned(
                child: NDock(
              widthDock: widthDock,
            )),
            Row(
              children: [
                SizedBox(
                  width: widthDock,
                ),
                Expanded(child: widget.body),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
