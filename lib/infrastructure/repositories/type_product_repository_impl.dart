import 'package:dartz/dartz.dart';
import 'package:lug_front/domain/feature/user_infos/entities/user_infos.dart';
import 'package:lug_front/infrastructure/api/rest_api_repository.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lug_front/infrastructure/dtos/type_product_dto.dart';
import 'package:lug_front/infrastructure/dtos/user_infos_dto.dart';

import '../../domain/feature/user/entities/user.dart';
import '../dtos/user_dto.dart';

class TypeProductRepositoryImpl extends RestApiRepository {
  TypeProductRepositoryImpl({
    required GetHttpClient client,
  }) : super(
          controller: '',
          client: client,
        );

  Future<Either<String, List<TypeProductDto>>> index() async {
    Response<dynamic> response = Response();
    response = await client.get("/typeProduct");
    if (response.hasError) {
      return left(response.body["message"]);
    } else {
      return right(response.body.map<TypeProductDto>((e) {
        return TypeProductDto.fromJson(e);
      }).toList());
    }
  }
}
