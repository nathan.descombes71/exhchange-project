import 'package:lug_front/infrastructure/api/rest_api_repository.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class UserInfosRepositoryImpl extends RestApiRepository {
  UserInfosRepositoryImpl({
    required GetHttpClient client,
  }) : super(
          controller: '',
          client: client,
        );
}
