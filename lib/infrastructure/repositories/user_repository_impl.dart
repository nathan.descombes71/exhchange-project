import 'package:dartz/dartz.dart';
import 'package:lug_front/domain/feature/user_infos/entities/user_infos.dart';
import 'package:lug_front/infrastructure/api/rest_api_repository.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lug_front/infrastructure/dtos/user_infos_dto.dart';

import '../../domain/feature/user/entities/user.dart';
import '../dtos/user_dto.dart';

class UserRepositoryImpl extends RestApiRepository {
  UserRepositoryImpl({
    required GetHttpClient client,
  }) : super(
          controller: '/users',
          client: client,
        );

  Future<Either<String, UserDto>> register(UserInfosDto userInfos) async {
    Response<dynamic> response = Response();

    response = await client.post("/auth/register", body: userInfos.toJson());

    if (response.hasError) {
      return left(response.body["message"]);
    } else {
      return right(UserDto.fromJson(response.body));
    }
  }

  Future<Either<String, UserDto>> login(String password, String mail) async {
    Response<dynamic> response = Response();

    response = await client.post("/auth/login",
        body: '{"email" : "$mail", "password" : "$password"}');

    if (response.hasError) {
      return left(response.body["message"]);
    } else {
      return right(UserDto.fromJson(response.body));
    }
  }

  Future<Either<String, UserInfosDto>> me() async {
    Response<dynamic> response = Response();
    response = await client.get("/me");
    if (response.hasError) {
      return left(response.body["message"]);
    } else {
      return right(UserInfosDto.fromJson(response.body));
    }
  }

  Future<Either<String, String>> logOut() async {
    Response<dynamic> response = Response();
    response = await client.post("/auth/logout");
    if (response.hasError) {
      return left(response.body["message"]);
    } else {
      return right(response.body["message"]);
    }
  }

  Future<Either<String, List<UserInfosDto>>> indexProducteur() async {
    Response<dynamic> response = Response();
    response = await client.get("/usersProducteurs");
    if (response.hasError) {
      return left(response.body["message"]);
    } else {
      return right(response.body.map<UserInfosDto>((e) {
        return UserInfosDto.fromJson(e);
      }).toList());
    }
  }

  Future<Either<UserInfosDto, UserInfosDto>> showUser(int userId) async {
    Response<dynamic> response = Response();
    response = await client.get("$controller/$userId");
    if (response.hasError) {
      return left(response.body["message"]);
    } else {
      return right(UserInfosDto.fromJson(response.body));
    }
  }
}
