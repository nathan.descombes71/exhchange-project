import 'package:get/get.dart';
import 'package:get/get_connect.dart';
import 'package:get/get_connect/http/src/request/request.dart';
import 'package:get_storage/get_storage.dart';

import 'rest_api_logger.dart';

class RestApiInterceptor extends RestApiLogger {
  Request requestModifier(Request request) {
    final box = GetStorage();

    String actualJWT = "";

    actualJWT = box.read("jwt") ?? "";
    request.headers.addAll({'Authorization': "Bearer " + actualJWT});

    onRequestLogger(request);
    return request;
  }

  Response responseModifier(Request request, Response response) {
    // TODO Edit request here //

    // ---------------------- //
    onResponseLogger(response);
    return response;
  }
}
