import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:lug_front/infrastructure/dtos/range_product_dto.dart';

part 'user_infos_dto.freezed.dart';
part 'user_infos_dto.g.dart';

@freezed
class UserInfosDto with _$UserInfosDto {
  const factory UserInfosDto({
    @JsonKey(name: 'id', includeIfNull: false) required int id,
    @JsonKey(name: 'nom') String? nom,
    @JsonKey(name: 'prenom') String? prenom,
    @JsonKey(name: 'email') String? email,
    @JsonKey(name: 'adresse') String? adresse,
    @JsonKey(name: 'password') String? password,
    @JsonKey(name: 'type') int? type,
    @JsonKey(name: 'description') String? description,
    @JsonKey(name: 'telephone') String? telephone,
    @JsonKey(name: 'pathPhoto') String? pathPhoto,
    @JsonKey(name: 'lat') double? lat,
    @JsonKey(name: 'lng') double? lng,
    @JsonKey(name: 'range_products') List<RangeProductDto>? listRangeProduct,
  }) = _UserInfosDto;
  factory UserInfosDto.fromJson(Map<String, dynamic> json) =>
      _$UserInfosDtoFromJson(json);
}

DateTime _fromJson(int int) => DateTime.fromMillisecondsSinceEpoch(int * 1000);

extension OnUserInfosJson on Map<String, dynamic> {
  UserInfosDto get toUserInfosDto {
    return UserInfosDto.fromJson(this);
  }
}

extension OnListUserInfosJson on List<Map<String, dynamic>> {
  List<UserInfosDto> get toUserInfosDto {
    return map<UserInfosDto>((Map<String, dynamic> e) => e.toUserInfosDto)
        .toList();
  }
}
