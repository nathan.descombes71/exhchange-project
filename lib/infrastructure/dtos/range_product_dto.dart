import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:lug_front/infrastructure/dtos/product_dto.dart';
import 'package:lug_front/infrastructure/dtos/type_product_dto.dart';

part 'range_product_dto.freezed.dart';
part 'range_product_dto.g.dart';

@freezed
class RangeProductDto with _$RangeProductDto {
  const factory RangeProductDto({
    @JsonKey(name: 'id', includeIfNull: false) required int id,
    @JsonKey(name: 'user_id') int? userId,
    @JsonKey(name: 'type_product_id') int? typeProductId,
    @JsonKey(name: 'pathPhoto') String? pathPhoto,
    @JsonKey(name: 'products') List<ProductDto>? listProduct,
    @JsonKey(name: 'type_products') TypeProductDto? typeProduct,
  }) = _RangeProductDto;
  factory RangeProductDto.fromJson(Map<String, dynamic> json) =>
      _$RangeProductDtoFromJson(json);
}

DateTime _fromJson(int int) => DateTime.fromMillisecondsSinceEpoch(int * 1000);

extension OnRangeProductInfosJson on Map<String, dynamic> {
  RangeProductDto get toRangeProductDto {
    return RangeProductDto.fromJson(this);
  }
}

extension OnListRangeProductInfosJson on List<Map<String, dynamic>> {
  List<RangeProductDto> get toRangeProductDto {
    return map<RangeProductDto>((Map<String, dynamic> e) => e.toRangeProductDto)
        .toList();
  }
}
