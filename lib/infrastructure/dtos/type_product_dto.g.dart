// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'type_product_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_TypeProductDto _$$_TypeProductDtoFromJson(Map<String, dynamic> json) =>
    _$_TypeProductDto(
      id: json['id'] as int,
      label: json['label'] as String?,
      pathPhoto: json['pathPhoto'] as String?,
    );

Map<String, dynamic> _$$_TypeProductDtoToJson(_$_TypeProductDto instance) =>
    <String, dynamic>{
      'id': instance.id,
      'label': instance.label,
      'pathPhoto': instance.pathPhoto,
    };
