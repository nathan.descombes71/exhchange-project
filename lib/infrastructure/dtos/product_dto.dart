import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:lug_front/infrastructure/dtos/range_product_dto.dart';

part 'product_dto.freezed.dart';
part 'product_dto.g.dart';

@freezed
class ProductDto with _$ProductDto {
  const factory ProductDto({
    @JsonKey(name: 'id', includeIfNull: false) required int id,
    @JsonKey(name: 'label') String? label,
    @JsonKey(name: 'prix') double? prix,
    @JsonKey(name: 'description') String? description,
    @JsonKey(name: 'pathPhoto') String? pathPhoto,
    @JsonKey(name: 'range_product_id') int? rangeProductId,
    @JsonKey(name: 'range_product') RangeProductDto? rangeProduct,
  }) = _ProductDto;
  factory ProductDto.fromJson(Map<String, dynamic> json) =>
      _$ProductDtoFromJson(json);
}

DateTime _fromJson(int int) => DateTime.fromMillisecondsSinceEpoch(int * 1000);

extension OnProductInfosJson on Map<String, dynamic> {
  ProductDto get toProductDto {
    return ProductDto.fromJson(this);
  }
}

extension OnListProductInfosJson on List<Map<String, dynamic>> {
  List<ProductDto> get toProductDto {
    return map<ProductDto>((Map<String, dynamic> e) => e.toProductDto).toList();
  }
}
