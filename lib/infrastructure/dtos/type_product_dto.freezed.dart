// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'type_product_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

TypeProductDto _$TypeProductDtoFromJson(Map<String, dynamic> json) {
  return _TypeProductDto.fromJson(json);
}

/// @nodoc
mixin _$TypeProductDto {
  @JsonKey(name: 'id', includeIfNull: false)
  int get id => throw _privateConstructorUsedError;
  @JsonKey(name: 'label')
  String? get label => throw _privateConstructorUsedError;
  @JsonKey(name: 'pathPhoto')
  String? get pathPhoto => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $TypeProductDtoCopyWith<TypeProductDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $TypeProductDtoCopyWith<$Res> {
  factory $TypeProductDtoCopyWith(
          TypeProductDto value, $Res Function(TypeProductDto) then) =
      _$TypeProductDtoCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'id', includeIfNull: false) int id,
      @JsonKey(name: 'label') String? label,
      @JsonKey(name: 'pathPhoto') String? pathPhoto});
}

/// @nodoc
class _$TypeProductDtoCopyWithImpl<$Res>
    implements $TypeProductDtoCopyWith<$Res> {
  _$TypeProductDtoCopyWithImpl(this._value, this._then);

  final TypeProductDto _value;
  // ignore: unused_field
  final $Res Function(TypeProductDto) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? label = freezed,
    Object? pathPhoto = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      label: label == freezed
          ? _value.label
          : label // ignore: cast_nullable_to_non_nullable
              as String?,
      pathPhoto: pathPhoto == freezed
          ? _value.pathPhoto
          : pathPhoto // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$$_TypeProductDtoCopyWith<$Res>
    implements $TypeProductDtoCopyWith<$Res> {
  factory _$$_TypeProductDtoCopyWith(
          _$_TypeProductDto value, $Res Function(_$_TypeProductDto) then) =
      __$$_TypeProductDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'id', includeIfNull: false) int id,
      @JsonKey(name: 'label') String? label,
      @JsonKey(name: 'pathPhoto') String? pathPhoto});
}

/// @nodoc
class __$$_TypeProductDtoCopyWithImpl<$Res>
    extends _$TypeProductDtoCopyWithImpl<$Res>
    implements _$$_TypeProductDtoCopyWith<$Res> {
  __$$_TypeProductDtoCopyWithImpl(
      _$_TypeProductDto _value, $Res Function(_$_TypeProductDto) _then)
      : super(_value, (v) => _then(v as _$_TypeProductDto));

  @override
  _$_TypeProductDto get _value => super._value as _$_TypeProductDto;

  @override
  $Res call({
    Object? id = freezed,
    Object? label = freezed,
    Object? pathPhoto = freezed,
  }) {
    return _then(_$_TypeProductDto(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      label: label == freezed
          ? _value.label
          : label // ignore: cast_nullable_to_non_nullable
              as String?,
      pathPhoto: pathPhoto == freezed
          ? _value.pathPhoto
          : pathPhoto // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_TypeProductDto implements _TypeProductDto {
  const _$_TypeProductDto(
      {@JsonKey(name: 'id', includeIfNull: false) required this.id,
      @JsonKey(name: 'label') this.label,
      @JsonKey(name: 'pathPhoto') this.pathPhoto});

  factory _$_TypeProductDto.fromJson(Map<String, dynamic> json) =>
      _$$_TypeProductDtoFromJson(json);

  @override
  @JsonKey(name: 'id', includeIfNull: false)
  final int id;
  @override
  @JsonKey(name: 'label')
  final String? label;
  @override
  @JsonKey(name: 'pathPhoto')
  final String? pathPhoto;

  @override
  String toString() {
    return 'TypeProductDto(id: $id, label: $label, pathPhoto: $pathPhoto)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_TypeProductDto &&
            const DeepCollectionEquality().equals(other.id, id) &&
            const DeepCollectionEquality().equals(other.label, label) &&
            const DeepCollectionEquality().equals(other.pathPhoto, pathPhoto));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(id),
      const DeepCollectionEquality().hash(label),
      const DeepCollectionEquality().hash(pathPhoto));

  @JsonKey(ignore: true)
  @override
  _$$_TypeProductDtoCopyWith<_$_TypeProductDto> get copyWith =>
      __$$_TypeProductDtoCopyWithImpl<_$_TypeProductDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_TypeProductDtoToJson(this);
  }
}

abstract class _TypeProductDto implements TypeProductDto {
  const factory _TypeProductDto(
      {@JsonKey(name: 'id', includeIfNull: false) required final int id,
      @JsonKey(name: 'label') final String? label,
      @JsonKey(name: 'pathPhoto') final String? pathPhoto}) = _$_TypeProductDto;

  factory _TypeProductDto.fromJson(Map<String, dynamic> json) =
      _$_TypeProductDto.fromJson;

  @override
  @JsonKey(name: 'id', includeIfNull: false)
  int get id;
  @override
  @JsonKey(name: 'label')
  String? get label;
  @override
  @JsonKey(name: 'pathPhoto')
  String? get pathPhoto;
  @override
  @JsonKey(ignore: true)
  _$$_TypeProductDtoCopyWith<_$_TypeProductDto> get copyWith =>
      throw _privateConstructorUsedError;
}
