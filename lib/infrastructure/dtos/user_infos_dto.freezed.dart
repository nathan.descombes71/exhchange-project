// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'user_infos_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

UserInfosDto _$UserInfosDtoFromJson(Map<String, dynamic> json) {
  return _UserInfosDto.fromJson(json);
}

/// @nodoc
mixin _$UserInfosDto {
  @JsonKey(name: 'id', includeIfNull: false)
  int get id => throw _privateConstructorUsedError;
  @JsonKey(name: 'nom')
  String? get nom => throw _privateConstructorUsedError;
  @JsonKey(name: 'prenom')
  String? get prenom => throw _privateConstructorUsedError;
  @JsonKey(name: 'email')
  String? get email => throw _privateConstructorUsedError;
  @JsonKey(name: 'adresse')
  String? get adresse => throw _privateConstructorUsedError;
  @JsonKey(name: 'password')
  String? get password => throw _privateConstructorUsedError;
  @JsonKey(name: 'type')
  int? get type => throw _privateConstructorUsedError;
  @JsonKey(name: 'description')
  String? get description => throw _privateConstructorUsedError;
  @JsonKey(name: 'telephone')
  String? get telephone => throw _privateConstructorUsedError;
  @JsonKey(name: 'pathPhoto')
  String? get pathPhoto => throw _privateConstructorUsedError;
  @JsonKey(name: 'lat')
  double? get lat => throw _privateConstructorUsedError;
  @JsonKey(name: 'lng')
  double? get lng => throw _privateConstructorUsedError;
  @JsonKey(name: 'range_products')
  List<RangeProductDto>? get listRangeProduct =>
      throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $UserInfosDtoCopyWith<UserInfosDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UserInfosDtoCopyWith<$Res> {
  factory $UserInfosDtoCopyWith(
          UserInfosDto value, $Res Function(UserInfosDto) then) =
      _$UserInfosDtoCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'id', includeIfNull: false)
          int id,
      @JsonKey(name: 'nom')
          String? nom,
      @JsonKey(name: 'prenom')
          String? prenom,
      @JsonKey(name: 'email')
          String? email,
      @JsonKey(name: 'adresse')
          String? adresse,
      @JsonKey(name: 'password')
          String? password,
      @JsonKey(name: 'type')
          int? type,
      @JsonKey(name: 'description')
          String? description,
      @JsonKey(name: 'telephone')
          String? telephone,
      @JsonKey(name: 'pathPhoto')
          String? pathPhoto,
      @JsonKey(name: 'lat')
          double? lat,
      @JsonKey(name: 'lng')
          double? lng,
      @JsonKey(name: 'range_products')
          List<RangeProductDto>? listRangeProduct});
}

/// @nodoc
class _$UserInfosDtoCopyWithImpl<$Res> implements $UserInfosDtoCopyWith<$Res> {
  _$UserInfosDtoCopyWithImpl(this._value, this._then);

  final UserInfosDto _value;
  // ignore: unused_field
  final $Res Function(UserInfosDto) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? nom = freezed,
    Object? prenom = freezed,
    Object? email = freezed,
    Object? adresse = freezed,
    Object? password = freezed,
    Object? type = freezed,
    Object? description = freezed,
    Object? telephone = freezed,
    Object? pathPhoto = freezed,
    Object? lat = freezed,
    Object? lng = freezed,
    Object? listRangeProduct = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      nom: nom == freezed
          ? _value.nom
          : nom // ignore: cast_nullable_to_non_nullable
              as String?,
      prenom: prenom == freezed
          ? _value.prenom
          : prenom // ignore: cast_nullable_to_non_nullable
              as String?,
      email: email == freezed
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      adresse: adresse == freezed
          ? _value.adresse
          : adresse // ignore: cast_nullable_to_non_nullable
              as String?,
      password: password == freezed
          ? _value.password
          : password // ignore: cast_nullable_to_non_nullable
              as String?,
      type: type == freezed
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as int?,
      description: description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      telephone: telephone == freezed
          ? _value.telephone
          : telephone // ignore: cast_nullable_to_non_nullable
              as String?,
      pathPhoto: pathPhoto == freezed
          ? _value.pathPhoto
          : pathPhoto // ignore: cast_nullable_to_non_nullable
              as String?,
      lat: lat == freezed
          ? _value.lat
          : lat // ignore: cast_nullable_to_non_nullable
              as double?,
      lng: lng == freezed
          ? _value.lng
          : lng // ignore: cast_nullable_to_non_nullable
              as double?,
      listRangeProduct: listRangeProduct == freezed
          ? _value.listRangeProduct
          : listRangeProduct // ignore: cast_nullable_to_non_nullable
              as List<RangeProductDto>?,
    ));
  }
}

/// @nodoc
abstract class _$$_UserInfosDtoCopyWith<$Res>
    implements $UserInfosDtoCopyWith<$Res> {
  factory _$$_UserInfosDtoCopyWith(
          _$_UserInfosDto value, $Res Function(_$_UserInfosDto) then) =
      __$$_UserInfosDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'id', includeIfNull: false)
          int id,
      @JsonKey(name: 'nom')
          String? nom,
      @JsonKey(name: 'prenom')
          String? prenom,
      @JsonKey(name: 'email')
          String? email,
      @JsonKey(name: 'adresse')
          String? adresse,
      @JsonKey(name: 'password')
          String? password,
      @JsonKey(name: 'type')
          int? type,
      @JsonKey(name: 'description')
          String? description,
      @JsonKey(name: 'telephone')
          String? telephone,
      @JsonKey(name: 'pathPhoto')
          String? pathPhoto,
      @JsonKey(name: 'lat')
          double? lat,
      @JsonKey(name: 'lng')
          double? lng,
      @JsonKey(name: 'range_products')
          List<RangeProductDto>? listRangeProduct});
}

/// @nodoc
class __$$_UserInfosDtoCopyWithImpl<$Res>
    extends _$UserInfosDtoCopyWithImpl<$Res>
    implements _$$_UserInfosDtoCopyWith<$Res> {
  __$$_UserInfosDtoCopyWithImpl(
      _$_UserInfosDto _value, $Res Function(_$_UserInfosDto) _then)
      : super(_value, (v) => _then(v as _$_UserInfosDto));

  @override
  _$_UserInfosDto get _value => super._value as _$_UserInfosDto;

  @override
  $Res call({
    Object? id = freezed,
    Object? nom = freezed,
    Object? prenom = freezed,
    Object? email = freezed,
    Object? adresse = freezed,
    Object? password = freezed,
    Object? type = freezed,
    Object? description = freezed,
    Object? telephone = freezed,
    Object? pathPhoto = freezed,
    Object? lat = freezed,
    Object? lng = freezed,
    Object? listRangeProduct = freezed,
  }) {
    return _then(_$_UserInfosDto(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      nom: nom == freezed
          ? _value.nom
          : nom // ignore: cast_nullable_to_non_nullable
              as String?,
      prenom: prenom == freezed
          ? _value.prenom
          : prenom // ignore: cast_nullable_to_non_nullable
              as String?,
      email: email == freezed
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      adresse: adresse == freezed
          ? _value.adresse
          : adresse // ignore: cast_nullable_to_non_nullable
              as String?,
      password: password == freezed
          ? _value.password
          : password // ignore: cast_nullable_to_non_nullable
              as String?,
      type: type == freezed
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as int?,
      description: description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      telephone: telephone == freezed
          ? _value.telephone
          : telephone // ignore: cast_nullable_to_non_nullable
              as String?,
      pathPhoto: pathPhoto == freezed
          ? _value.pathPhoto
          : pathPhoto // ignore: cast_nullable_to_non_nullable
              as String?,
      lat: lat == freezed
          ? _value.lat
          : lat // ignore: cast_nullable_to_non_nullable
              as double?,
      lng: lng == freezed
          ? _value.lng
          : lng // ignore: cast_nullable_to_non_nullable
              as double?,
      listRangeProduct: listRangeProduct == freezed
          ? _value._listRangeProduct
          : listRangeProduct // ignore: cast_nullable_to_non_nullable
              as List<RangeProductDto>?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_UserInfosDto implements _UserInfosDto {
  const _$_UserInfosDto(
      {@JsonKey(name: 'id', includeIfNull: false)
          required this.id,
      @JsonKey(name: 'nom')
          this.nom,
      @JsonKey(name: 'prenom')
          this.prenom,
      @JsonKey(name: 'email')
          this.email,
      @JsonKey(name: 'adresse')
          this.adresse,
      @JsonKey(name: 'password')
          this.password,
      @JsonKey(name: 'type')
          this.type,
      @JsonKey(name: 'description')
          this.description,
      @JsonKey(name: 'telephone')
          this.telephone,
      @JsonKey(name: 'pathPhoto')
          this.pathPhoto,
      @JsonKey(name: 'lat')
          this.lat,
      @JsonKey(name: 'lng')
          this.lng,
      @JsonKey(name: 'range_products')
          final List<RangeProductDto>? listRangeProduct})
      : _listRangeProduct = listRangeProduct;

  factory _$_UserInfosDto.fromJson(Map<String, dynamic> json) =>
      _$$_UserInfosDtoFromJson(json);

  @override
  @JsonKey(name: 'id', includeIfNull: false)
  final int id;
  @override
  @JsonKey(name: 'nom')
  final String? nom;
  @override
  @JsonKey(name: 'prenom')
  final String? prenom;
  @override
  @JsonKey(name: 'email')
  final String? email;
  @override
  @JsonKey(name: 'adresse')
  final String? adresse;
  @override
  @JsonKey(name: 'password')
  final String? password;
  @override
  @JsonKey(name: 'type')
  final int? type;
  @override
  @JsonKey(name: 'description')
  final String? description;
  @override
  @JsonKey(name: 'telephone')
  final String? telephone;
  @override
  @JsonKey(name: 'pathPhoto')
  final String? pathPhoto;
  @override
  @JsonKey(name: 'lat')
  final double? lat;
  @override
  @JsonKey(name: 'lng')
  final double? lng;
  final List<RangeProductDto>? _listRangeProduct;
  @override
  @JsonKey(name: 'range_products')
  List<RangeProductDto>? get listRangeProduct {
    final value = _listRangeProduct;
    if (value == null) return null;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  String toString() {
    return 'UserInfosDto(id: $id, nom: $nom, prenom: $prenom, email: $email, adresse: $adresse, password: $password, type: $type, description: $description, telephone: $telephone, pathPhoto: $pathPhoto, lat: $lat, lng: $lng, listRangeProduct: $listRangeProduct)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_UserInfosDto &&
            const DeepCollectionEquality().equals(other.id, id) &&
            const DeepCollectionEquality().equals(other.nom, nom) &&
            const DeepCollectionEquality().equals(other.prenom, prenom) &&
            const DeepCollectionEquality().equals(other.email, email) &&
            const DeepCollectionEquality().equals(other.adresse, adresse) &&
            const DeepCollectionEquality().equals(other.password, password) &&
            const DeepCollectionEquality().equals(other.type, type) &&
            const DeepCollectionEquality()
                .equals(other.description, description) &&
            const DeepCollectionEquality().equals(other.telephone, telephone) &&
            const DeepCollectionEquality().equals(other.pathPhoto, pathPhoto) &&
            const DeepCollectionEquality().equals(other.lat, lat) &&
            const DeepCollectionEquality().equals(other.lng, lng) &&
            const DeepCollectionEquality()
                .equals(other._listRangeProduct, _listRangeProduct));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(id),
      const DeepCollectionEquality().hash(nom),
      const DeepCollectionEquality().hash(prenom),
      const DeepCollectionEquality().hash(email),
      const DeepCollectionEquality().hash(adresse),
      const DeepCollectionEquality().hash(password),
      const DeepCollectionEquality().hash(type),
      const DeepCollectionEquality().hash(description),
      const DeepCollectionEquality().hash(telephone),
      const DeepCollectionEquality().hash(pathPhoto),
      const DeepCollectionEquality().hash(lat),
      const DeepCollectionEquality().hash(lng),
      const DeepCollectionEquality().hash(_listRangeProduct));

  @JsonKey(ignore: true)
  @override
  _$$_UserInfosDtoCopyWith<_$_UserInfosDto> get copyWith =>
      __$$_UserInfosDtoCopyWithImpl<_$_UserInfosDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_UserInfosDtoToJson(this);
  }
}

abstract class _UserInfosDto implements UserInfosDto {
  const factory _UserInfosDto(
      {@JsonKey(name: 'id', includeIfNull: false)
          required final int id,
      @JsonKey(name: 'nom')
          final String? nom,
      @JsonKey(name: 'prenom')
          final String? prenom,
      @JsonKey(name: 'email')
          final String? email,
      @JsonKey(name: 'adresse')
          final String? adresse,
      @JsonKey(name: 'password')
          final String? password,
      @JsonKey(name: 'type')
          final int? type,
      @JsonKey(name: 'description')
          final String? description,
      @JsonKey(name: 'telephone')
          final String? telephone,
      @JsonKey(name: 'pathPhoto')
          final String? pathPhoto,
      @JsonKey(name: 'lat')
          final double? lat,
      @JsonKey(name: 'lng')
          final double? lng,
      @JsonKey(name: 'range_products')
          final List<RangeProductDto>? listRangeProduct}) = _$_UserInfosDto;

  factory _UserInfosDto.fromJson(Map<String, dynamic> json) =
      _$_UserInfosDto.fromJson;

  @override
  @JsonKey(name: 'id', includeIfNull: false)
  int get id;
  @override
  @JsonKey(name: 'nom')
  String? get nom;
  @override
  @JsonKey(name: 'prenom')
  String? get prenom;
  @override
  @JsonKey(name: 'email')
  String? get email;
  @override
  @JsonKey(name: 'adresse')
  String? get adresse;
  @override
  @JsonKey(name: 'password')
  String? get password;
  @override
  @JsonKey(name: 'type')
  int? get type;
  @override
  @JsonKey(name: 'description')
  String? get description;
  @override
  @JsonKey(name: 'telephone')
  String? get telephone;
  @override
  @JsonKey(name: 'pathPhoto')
  String? get pathPhoto;
  @override
  @JsonKey(name: 'lat')
  double? get lat;
  @override
  @JsonKey(name: 'lng')
  double? get lng;
  @override
  @JsonKey(name: 'range_products')
  List<RangeProductDto>? get listRangeProduct;
  @override
  @JsonKey(ignore: true)
  _$$_UserInfosDtoCopyWith<_$_UserInfosDto> get copyWith =>
      throw _privateConstructorUsedError;
}
