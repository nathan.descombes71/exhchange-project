import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:lug_front/infrastructure/dtos/user_infos_dto.dart';

part 'user_dto.freezed.dart';
part 'user_dto.g.dart';

@freezed
class UserDto with _$UserDto {
  const factory UserDto({
    @JsonKey(name: 'token') String? jwt,
    @JsonKey(name: 'user') UserInfosDto? userInfos,
  }) = _UserDto;
  factory UserDto.fromJson(Map<String, dynamic> json) =>
      _$UserDtoFromJson(json);
}

DateTime _fromJson(int int) => DateTime.fromMillisecondsSinceEpoch(int * 1000);

extension OnUserInfosJson on Map<String, dynamic> {
  UserDto get toUserDto {
    return UserDto.fromJson(this);
  }
}

extension OnListUserInfosJson on List<Map<String, dynamic>> {
  List<UserDto> get toUserDto {
    return map<UserDto>((Map<String, dynamic> e) => e.toUserDto).toList();
  }
}
