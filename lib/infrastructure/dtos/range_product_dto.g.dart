// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'range_product_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_RangeProductDto _$$_RangeProductDtoFromJson(Map<String, dynamic> json) =>
    _$_RangeProductDto(
      id: json['id'] as int,
      userId: json['user_id'] as int?,
      typeProductId: json['type_product_id'] as int?,
      pathPhoto: json['pathPhoto'] as String?,
      listProduct: (json['products'] as List<dynamic>?)
          ?.map((e) => ProductDto.fromJson(e as Map<String, dynamic>))
          .toList(),
      typeProduct: json['type_products'] == null
          ? null
          : TypeProductDto.fromJson(
              json['type_products'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$_RangeProductDtoToJson(_$_RangeProductDto instance) =>
    <String, dynamic>{
      'id': instance.id,
      'user_id': instance.userId,
      'type_product_id': instance.typeProductId,
      'pathPhoto': instance.pathPhoto,
      'products': instance.listProduct,
      'type_products': instance.typeProduct,
    };
