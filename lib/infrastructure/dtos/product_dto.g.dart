// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ProductDto _$$_ProductDtoFromJson(Map<String, dynamic> json) =>
    _$_ProductDto(
      id: json['id'] as int,
      label: json['label'] as String?,
      prix: (json['prix'] as num?)?.toDouble(),
      description: json['description'] as String?,
      pathPhoto: json['pathPhoto'] as String?,
      rangeProductId: json['range_product_id'] as int?,
      rangeProduct: json['range_product'] == null
          ? null
          : RangeProductDto.fromJson(
              json['range_product'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$_ProductDtoToJson(_$_ProductDto instance) =>
    <String, dynamic>{
      'id': instance.id,
      'label': instance.label,
      'prix': instance.prix,
      'description': instance.description,
      'pathPhoto': instance.pathPhoto,
      'range_product_id': instance.rangeProductId,
      'range_product': instance.rangeProduct,
    };
