// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_infos_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_UserInfosDto _$$_UserInfosDtoFromJson(Map<String, dynamic> json) =>
    _$_UserInfosDto(
      id: json['id'] as int,
      nom: json['nom'] as String?,
      prenom: json['prenom'] as String?,
      email: json['email'] as String?,
      adresse: json['adresse'] as String?,
      password: json['password'] as String?,
      type: json['type'] as int?,
      description: json['description'] as String?,
      telephone: json['telephone'] as String?,
      pathPhoto: json['pathPhoto'] as String?,
      lat: (json['lat'] as num?)?.toDouble(),
      lng: (json['lng'] as num?)?.toDouble(),
      listRangeProduct: (json['range_products'] as List<dynamic>?)
          ?.map((e) => RangeProductDto.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_UserInfosDtoToJson(_$_UserInfosDto instance) =>
    <String, dynamic>{
      'id': instance.id,
      'nom': instance.nom,
      'prenom': instance.prenom,
      'email': instance.email,
      'adresse': instance.adresse,
      'password': instance.password,
      'type': instance.type,
      'description': instance.description,
      'telephone': instance.telephone,
      'pathPhoto': instance.pathPhoto,
      'lat': instance.lat,
      'lng': instance.lng,
      'range_products': instance.listRangeProduct,
    };
