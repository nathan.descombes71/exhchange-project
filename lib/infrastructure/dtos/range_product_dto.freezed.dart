// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'range_product_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

RangeProductDto _$RangeProductDtoFromJson(Map<String, dynamic> json) {
  return _RangeProductDto.fromJson(json);
}

/// @nodoc
mixin _$RangeProductDto {
  @JsonKey(name: 'id', includeIfNull: false)
  int get id => throw _privateConstructorUsedError;
  @JsonKey(name: 'user_id')
  int? get userId => throw _privateConstructorUsedError;
  @JsonKey(name: 'type_product_id')
  int? get typeProductId => throw _privateConstructorUsedError;
  @JsonKey(name: 'pathPhoto')
  String? get pathPhoto => throw _privateConstructorUsedError;
  @JsonKey(name: 'products')
  List<ProductDto>? get listProduct => throw _privateConstructorUsedError;
  @JsonKey(name: 'type_products')
  TypeProductDto? get typeProduct => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $RangeProductDtoCopyWith<RangeProductDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RangeProductDtoCopyWith<$Res> {
  factory $RangeProductDtoCopyWith(
          RangeProductDto value, $Res Function(RangeProductDto) then) =
      _$RangeProductDtoCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'id', includeIfNull: false) int id,
      @JsonKey(name: 'user_id') int? userId,
      @JsonKey(name: 'type_product_id') int? typeProductId,
      @JsonKey(name: 'pathPhoto') String? pathPhoto,
      @JsonKey(name: 'products') List<ProductDto>? listProduct,
      @JsonKey(name: 'type_products') TypeProductDto? typeProduct});

  $TypeProductDtoCopyWith<$Res>? get typeProduct;
}

/// @nodoc
class _$RangeProductDtoCopyWithImpl<$Res>
    implements $RangeProductDtoCopyWith<$Res> {
  _$RangeProductDtoCopyWithImpl(this._value, this._then);

  final RangeProductDto _value;
  // ignore: unused_field
  final $Res Function(RangeProductDto) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? userId = freezed,
    Object? typeProductId = freezed,
    Object? pathPhoto = freezed,
    Object? listProduct = freezed,
    Object? typeProduct = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      userId: userId == freezed
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as int?,
      typeProductId: typeProductId == freezed
          ? _value.typeProductId
          : typeProductId // ignore: cast_nullable_to_non_nullable
              as int?,
      pathPhoto: pathPhoto == freezed
          ? _value.pathPhoto
          : pathPhoto // ignore: cast_nullable_to_non_nullable
              as String?,
      listProduct: listProduct == freezed
          ? _value.listProduct
          : listProduct // ignore: cast_nullable_to_non_nullable
              as List<ProductDto>?,
      typeProduct: typeProduct == freezed
          ? _value.typeProduct
          : typeProduct // ignore: cast_nullable_to_non_nullable
              as TypeProductDto?,
    ));
  }

  @override
  $TypeProductDtoCopyWith<$Res>? get typeProduct {
    if (_value.typeProduct == null) {
      return null;
    }

    return $TypeProductDtoCopyWith<$Res>(_value.typeProduct!, (value) {
      return _then(_value.copyWith(typeProduct: value));
    });
  }
}

/// @nodoc
abstract class _$$_RangeProductDtoCopyWith<$Res>
    implements $RangeProductDtoCopyWith<$Res> {
  factory _$$_RangeProductDtoCopyWith(
          _$_RangeProductDto value, $Res Function(_$_RangeProductDto) then) =
      __$$_RangeProductDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'id', includeIfNull: false) int id,
      @JsonKey(name: 'user_id') int? userId,
      @JsonKey(name: 'type_product_id') int? typeProductId,
      @JsonKey(name: 'pathPhoto') String? pathPhoto,
      @JsonKey(name: 'products') List<ProductDto>? listProduct,
      @JsonKey(name: 'type_products') TypeProductDto? typeProduct});

  @override
  $TypeProductDtoCopyWith<$Res>? get typeProduct;
}

/// @nodoc
class __$$_RangeProductDtoCopyWithImpl<$Res>
    extends _$RangeProductDtoCopyWithImpl<$Res>
    implements _$$_RangeProductDtoCopyWith<$Res> {
  __$$_RangeProductDtoCopyWithImpl(
      _$_RangeProductDto _value, $Res Function(_$_RangeProductDto) _then)
      : super(_value, (v) => _then(v as _$_RangeProductDto));

  @override
  _$_RangeProductDto get _value => super._value as _$_RangeProductDto;

  @override
  $Res call({
    Object? id = freezed,
    Object? userId = freezed,
    Object? typeProductId = freezed,
    Object? pathPhoto = freezed,
    Object? listProduct = freezed,
    Object? typeProduct = freezed,
  }) {
    return _then(_$_RangeProductDto(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      userId: userId == freezed
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as int?,
      typeProductId: typeProductId == freezed
          ? _value.typeProductId
          : typeProductId // ignore: cast_nullable_to_non_nullable
              as int?,
      pathPhoto: pathPhoto == freezed
          ? _value.pathPhoto
          : pathPhoto // ignore: cast_nullable_to_non_nullable
              as String?,
      listProduct: listProduct == freezed
          ? _value._listProduct
          : listProduct // ignore: cast_nullable_to_non_nullable
              as List<ProductDto>?,
      typeProduct: typeProduct == freezed
          ? _value.typeProduct
          : typeProduct // ignore: cast_nullable_to_non_nullable
              as TypeProductDto?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_RangeProductDto implements _RangeProductDto {
  const _$_RangeProductDto(
      {@JsonKey(name: 'id', includeIfNull: false) required this.id,
      @JsonKey(name: 'user_id') this.userId,
      @JsonKey(name: 'type_product_id') this.typeProductId,
      @JsonKey(name: 'pathPhoto') this.pathPhoto,
      @JsonKey(name: 'products') final List<ProductDto>? listProduct,
      @JsonKey(name: 'type_products') this.typeProduct})
      : _listProduct = listProduct;

  factory _$_RangeProductDto.fromJson(Map<String, dynamic> json) =>
      _$$_RangeProductDtoFromJson(json);

  @override
  @JsonKey(name: 'id', includeIfNull: false)
  final int id;
  @override
  @JsonKey(name: 'user_id')
  final int? userId;
  @override
  @JsonKey(name: 'type_product_id')
  final int? typeProductId;
  @override
  @JsonKey(name: 'pathPhoto')
  final String? pathPhoto;
  final List<ProductDto>? _listProduct;
  @override
  @JsonKey(name: 'products')
  List<ProductDto>? get listProduct {
    final value = _listProduct;
    if (value == null) return null;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  @JsonKey(name: 'type_products')
  final TypeProductDto? typeProduct;

  @override
  String toString() {
    return 'RangeProductDto(id: $id, userId: $userId, typeProductId: $typeProductId, pathPhoto: $pathPhoto, listProduct: $listProduct, typeProduct: $typeProduct)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_RangeProductDto &&
            const DeepCollectionEquality().equals(other.id, id) &&
            const DeepCollectionEquality().equals(other.userId, userId) &&
            const DeepCollectionEquality()
                .equals(other.typeProductId, typeProductId) &&
            const DeepCollectionEquality().equals(other.pathPhoto, pathPhoto) &&
            const DeepCollectionEquality()
                .equals(other._listProduct, _listProduct) &&
            const DeepCollectionEquality()
                .equals(other.typeProduct, typeProduct));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(id),
      const DeepCollectionEquality().hash(userId),
      const DeepCollectionEquality().hash(typeProductId),
      const DeepCollectionEquality().hash(pathPhoto),
      const DeepCollectionEquality().hash(_listProduct),
      const DeepCollectionEquality().hash(typeProduct));

  @JsonKey(ignore: true)
  @override
  _$$_RangeProductDtoCopyWith<_$_RangeProductDto> get copyWith =>
      __$$_RangeProductDtoCopyWithImpl<_$_RangeProductDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_RangeProductDtoToJson(this);
  }
}

abstract class _RangeProductDto implements RangeProductDto {
  const factory _RangeProductDto(
          {@JsonKey(name: 'id', includeIfNull: false) required final int id,
          @JsonKey(name: 'user_id') final int? userId,
          @JsonKey(name: 'type_product_id') final int? typeProductId,
          @JsonKey(name: 'pathPhoto') final String? pathPhoto,
          @JsonKey(name: 'products') final List<ProductDto>? listProduct,
          @JsonKey(name: 'type_products') final TypeProductDto? typeProduct}) =
      _$_RangeProductDto;

  factory _RangeProductDto.fromJson(Map<String, dynamic> json) =
      _$_RangeProductDto.fromJson;

  @override
  @JsonKey(name: 'id', includeIfNull: false)
  int get id;
  @override
  @JsonKey(name: 'user_id')
  int? get userId;
  @override
  @JsonKey(name: 'type_product_id')
  int? get typeProductId;
  @override
  @JsonKey(name: 'pathPhoto')
  String? get pathPhoto;
  @override
  @JsonKey(name: 'products')
  List<ProductDto>? get listProduct;
  @override
  @JsonKey(name: 'type_products')
  TypeProductDto? get typeProduct;
  @override
  @JsonKey(ignore: true)
  _$$_RangeProductDtoCopyWith<_$_RangeProductDto> get copyWith =>
      throw _privateConstructorUsedError;
}
