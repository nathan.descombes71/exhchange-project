// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'product_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ProductDto _$ProductDtoFromJson(Map<String, dynamic> json) {
  return _ProductDto.fromJson(json);
}

/// @nodoc
mixin _$ProductDto {
  @JsonKey(name: 'id', includeIfNull: false)
  int get id => throw _privateConstructorUsedError;
  @JsonKey(name: 'label')
  String? get label => throw _privateConstructorUsedError;
  @JsonKey(name: 'prix')
  double? get prix => throw _privateConstructorUsedError;
  @JsonKey(name: 'description')
  String? get description => throw _privateConstructorUsedError;
  @JsonKey(name: 'pathPhoto')
  String? get pathPhoto => throw _privateConstructorUsedError;
  @JsonKey(name: 'range_product_id')
  int? get rangeProductId => throw _privateConstructorUsedError;
  @JsonKey(name: 'range_product')
  RangeProductDto? get rangeProduct => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ProductDtoCopyWith<ProductDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProductDtoCopyWith<$Res> {
  factory $ProductDtoCopyWith(
          ProductDto value, $Res Function(ProductDto) then) =
      _$ProductDtoCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'id', includeIfNull: false) int id,
      @JsonKey(name: 'label') String? label,
      @JsonKey(name: 'prix') double? prix,
      @JsonKey(name: 'description') String? description,
      @JsonKey(name: 'pathPhoto') String? pathPhoto,
      @JsonKey(name: 'range_product_id') int? rangeProductId,
      @JsonKey(name: 'range_product') RangeProductDto? rangeProduct});

  $RangeProductDtoCopyWith<$Res>? get rangeProduct;
}

/// @nodoc
class _$ProductDtoCopyWithImpl<$Res> implements $ProductDtoCopyWith<$Res> {
  _$ProductDtoCopyWithImpl(this._value, this._then);

  final ProductDto _value;
  // ignore: unused_field
  final $Res Function(ProductDto) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? label = freezed,
    Object? prix = freezed,
    Object? description = freezed,
    Object? pathPhoto = freezed,
    Object? rangeProductId = freezed,
    Object? rangeProduct = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      label: label == freezed
          ? _value.label
          : label // ignore: cast_nullable_to_non_nullable
              as String?,
      prix: prix == freezed
          ? _value.prix
          : prix // ignore: cast_nullable_to_non_nullable
              as double?,
      description: description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      pathPhoto: pathPhoto == freezed
          ? _value.pathPhoto
          : pathPhoto // ignore: cast_nullable_to_non_nullable
              as String?,
      rangeProductId: rangeProductId == freezed
          ? _value.rangeProductId
          : rangeProductId // ignore: cast_nullable_to_non_nullable
              as int?,
      rangeProduct: rangeProduct == freezed
          ? _value.rangeProduct
          : rangeProduct // ignore: cast_nullable_to_non_nullable
              as RangeProductDto?,
    ));
  }

  @override
  $RangeProductDtoCopyWith<$Res>? get rangeProduct {
    if (_value.rangeProduct == null) {
      return null;
    }

    return $RangeProductDtoCopyWith<$Res>(_value.rangeProduct!, (value) {
      return _then(_value.copyWith(rangeProduct: value));
    });
  }
}

/// @nodoc
abstract class _$$_ProductDtoCopyWith<$Res>
    implements $ProductDtoCopyWith<$Res> {
  factory _$$_ProductDtoCopyWith(
          _$_ProductDto value, $Res Function(_$_ProductDto) then) =
      __$$_ProductDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'id', includeIfNull: false) int id,
      @JsonKey(name: 'label') String? label,
      @JsonKey(name: 'prix') double? prix,
      @JsonKey(name: 'description') String? description,
      @JsonKey(name: 'pathPhoto') String? pathPhoto,
      @JsonKey(name: 'range_product_id') int? rangeProductId,
      @JsonKey(name: 'range_product') RangeProductDto? rangeProduct});

  @override
  $RangeProductDtoCopyWith<$Res>? get rangeProduct;
}

/// @nodoc
class __$$_ProductDtoCopyWithImpl<$Res> extends _$ProductDtoCopyWithImpl<$Res>
    implements _$$_ProductDtoCopyWith<$Res> {
  __$$_ProductDtoCopyWithImpl(
      _$_ProductDto _value, $Res Function(_$_ProductDto) _then)
      : super(_value, (v) => _then(v as _$_ProductDto));

  @override
  _$_ProductDto get _value => super._value as _$_ProductDto;

  @override
  $Res call({
    Object? id = freezed,
    Object? label = freezed,
    Object? prix = freezed,
    Object? description = freezed,
    Object? pathPhoto = freezed,
    Object? rangeProductId = freezed,
    Object? rangeProduct = freezed,
  }) {
    return _then(_$_ProductDto(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      label: label == freezed
          ? _value.label
          : label // ignore: cast_nullable_to_non_nullable
              as String?,
      prix: prix == freezed
          ? _value.prix
          : prix // ignore: cast_nullable_to_non_nullable
              as double?,
      description: description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      pathPhoto: pathPhoto == freezed
          ? _value.pathPhoto
          : pathPhoto // ignore: cast_nullable_to_non_nullable
              as String?,
      rangeProductId: rangeProductId == freezed
          ? _value.rangeProductId
          : rangeProductId // ignore: cast_nullable_to_non_nullable
              as int?,
      rangeProduct: rangeProduct == freezed
          ? _value.rangeProduct
          : rangeProduct // ignore: cast_nullable_to_non_nullable
              as RangeProductDto?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ProductDto implements _ProductDto {
  const _$_ProductDto(
      {@JsonKey(name: 'id', includeIfNull: false) required this.id,
      @JsonKey(name: 'label') this.label,
      @JsonKey(name: 'prix') this.prix,
      @JsonKey(name: 'description') this.description,
      @JsonKey(name: 'pathPhoto') this.pathPhoto,
      @JsonKey(name: 'range_product_id') this.rangeProductId,
      @JsonKey(name: 'range_product') this.rangeProduct});

  factory _$_ProductDto.fromJson(Map<String, dynamic> json) =>
      _$$_ProductDtoFromJson(json);

  @override
  @JsonKey(name: 'id', includeIfNull: false)
  final int id;
  @override
  @JsonKey(name: 'label')
  final String? label;
  @override
  @JsonKey(name: 'prix')
  final double? prix;
  @override
  @JsonKey(name: 'description')
  final String? description;
  @override
  @JsonKey(name: 'pathPhoto')
  final String? pathPhoto;
  @override
  @JsonKey(name: 'range_product_id')
  final int? rangeProductId;
  @override
  @JsonKey(name: 'range_product')
  final RangeProductDto? rangeProduct;

  @override
  String toString() {
    return 'ProductDto(id: $id, label: $label, prix: $prix, description: $description, pathPhoto: $pathPhoto, rangeProductId: $rangeProductId, rangeProduct: $rangeProduct)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ProductDto &&
            const DeepCollectionEquality().equals(other.id, id) &&
            const DeepCollectionEquality().equals(other.label, label) &&
            const DeepCollectionEquality().equals(other.prix, prix) &&
            const DeepCollectionEquality()
                .equals(other.description, description) &&
            const DeepCollectionEquality().equals(other.pathPhoto, pathPhoto) &&
            const DeepCollectionEquality()
                .equals(other.rangeProductId, rangeProductId) &&
            const DeepCollectionEquality()
                .equals(other.rangeProduct, rangeProduct));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(id),
      const DeepCollectionEquality().hash(label),
      const DeepCollectionEquality().hash(prix),
      const DeepCollectionEquality().hash(description),
      const DeepCollectionEquality().hash(pathPhoto),
      const DeepCollectionEquality().hash(rangeProductId),
      const DeepCollectionEquality().hash(rangeProduct));

  @JsonKey(ignore: true)
  @override
  _$$_ProductDtoCopyWith<_$_ProductDto> get copyWith =>
      __$$_ProductDtoCopyWithImpl<_$_ProductDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ProductDtoToJson(this);
  }
}

abstract class _ProductDto implements ProductDto {
  const factory _ProductDto(
      {@JsonKey(name: 'id', includeIfNull: false)
          required final int id,
      @JsonKey(name: 'label')
          final String? label,
      @JsonKey(name: 'prix')
          final double? prix,
      @JsonKey(name: 'description')
          final String? description,
      @JsonKey(name: 'pathPhoto')
          final String? pathPhoto,
      @JsonKey(name: 'range_product_id')
          final int? rangeProductId,
      @JsonKey(name: 'range_product')
          final RangeProductDto? rangeProduct}) = _$_ProductDto;

  factory _ProductDto.fromJson(Map<String, dynamic> json) =
      _$_ProductDto.fromJson;

  @override
  @JsonKey(name: 'id', includeIfNull: false)
  int get id;
  @override
  @JsonKey(name: 'label')
  String? get label;
  @override
  @JsonKey(name: 'prix')
  double? get prix;
  @override
  @JsonKey(name: 'description')
  String? get description;
  @override
  @JsonKey(name: 'pathPhoto')
  String? get pathPhoto;
  @override
  @JsonKey(name: 'range_product_id')
  int? get rangeProductId;
  @override
  @JsonKey(name: 'range_product')
  RangeProductDto? get rangeProduct;
  @override
  @JsonKey(ignore: true)
  _$$_ProductDtoCopyWith<_$_ProductDto> get copyWith =>
      throw _privateConstructorUsedError;
}
