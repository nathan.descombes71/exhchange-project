import 'package:freezed_annotation/freezed_annotation.dart';

part 'type_product_dto.freezed.dart';
part 'type_product_dto.g.dart';

@freezed
class TypeProductDto with _$TypeProductDto {
  const factory TypeProductDto({
    @JsonKey(name: 'id', includeIfNull: false) required int id,
    @JsonKey(name: 'label') String? label,
    @JsonKey(name: 'pathPhoto') String? pathPhoto,
  }) = _TypeProductDto;
  factory TypeProductDto.fromJson(Map<String, dynamic> json) =>
      _$TypeProductDtoFromJson(json);
}

DateTime _fromJson(int int) => DateTime.fromMillisecondsSinceEpoch(int * 1000);

extension OnTypeProductInfosJson on Map<String, dynamic> {
  TypeProductDto get toTypeProductDto {
    return TypeProductDto.fromJson(this);
  }
}

extension OnListTypeProductInfosJson on List<Map<String, dynamic>> {
  List<TypeProductDto> get toTypeProductDto {
    return map<TypeProductDto>((Map<String, dynamic> e) => e.toTypeProductDto)
        .toList();
  }
}
