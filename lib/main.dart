import 'package:lug_front/domain/feature/user/controller/user_controller.dart';
import 'package:lug_front/domain/feature/user/entities/user.dart';
import 'package:lug_front/infrastructure/api/rest_api_interceptor.dart';
import 'package:lug_front/infrastructure/repositories/user_repository_impl.dart';
import 'package:lug_front/presentation/core/styles/theme_datas.dart';
import 'package:lug_front/presentation/core/translations/localization_service.dart';
import 'package:lug_front/presentation/navigation/navigation.dart';
import 'package:lug_front/presentation/navigation/routes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import 'infrastructure/api/rest_api_client.dart';

// import 'package:flutter_localizations/flutter_localizations.dart';

String initialRoute = Routes.initialRoute;
void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await initServices();

  // String initialRoute = Routes.initialRoute;
  runApp(
    GetMaterialApp(
      title: 'LUG',
      initialRoute: initialRoute,
      getPages: Nav.routes,
      theme: XThemeData.light(),
      darkTheme: XThemeData.dark(),
      themeMode: ThemeMode.light,
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      locale: Locale('fr', 'FR'),
      supportedLocales: const <Locale>[
        Locale('fr', 'FR'),
        Locale('en', 'US'),
      ],
      fallbackLocale: Locale('fr', 'FR'),
      translations: LocalizationService(),
      onReady: () {
        if (initialRoute == Routes.REGISTER) {
          Get.offAllNamed(Routes.REGISTER);
        } else if (initialRoute == Routes.HOME) {
          // Get.offAllNamed(Routes.HOME);
        }
      },
    ),
  );
}

Future<void> initServices() async {
  /// Here is where you put get_storage, hive, shared_pref initialization.
  /// or moor connection, or whatever that's async.
  await GetStorage.init();

  // initialRoute = Routes.REGISTER;

  Get.put(
    RestApiClient(
      restApiInterceptor: Get.put(
        RestApiInterceptor(),
      ),
    ),
    permanent: true,
  );

  Get.put(
    UserController(
      userRepository: Get.put(
        UserRepositoryImpl(client: Get.find<RestApiClient>().client),
        permanent: true,
      ),
    ),
    permanent: true,
  );

  final box = GetStorage();

  String jwtExist = box.read("jwt") ?? "";

  if (jwtExist.isNotEmpty) {
    UserController userController = Get.find<UserController>();

    await userController.me().then(
      (value) {
        value.fold((l) {
          // initialRoute = Routes.REGISTER;
          initialRoute = Routes.REGISTER;
        }, (r) {
          Get.put<User>(User(userInfos: r));
          // initialRoute = Routes.HOME;
        });
      },
    );
  } else {
    initialRoute = Routes.REGISTER;
  }

  print('All services started !');
}
